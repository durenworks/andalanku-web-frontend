<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('index');

Route::get('/about', function () {
    return view('landing_page.about');
})->name('about');

Route::get('/news', 'PageController@newsPage')->name('news');

Route::get('/promo', 'PageController@promoPage')->name('promo');

Route::get('/contact', 'ContactController@index')->name('contact');
Route::get('/get_branch', 'ContactController@getBranch')->name('contact.get_branch');

Route::get('/services', function () {
    return view('landing_page.services');
})->name('services');

Route::get('/service/claim', function () {
    return view('landing_page.service-eclaim');
})->name('eclaim');

Route::get('/service/complain', function () {
    return view('landing_page.service-ecomplain');
})->name('ecomplain');

Route::get('/service/request', function () {
    return view('landing_page.service-erequest');
})->name('erequest');

Route::get('/news/{slug}', 'PageController@newsDetail')->name('news.detail');

Route::get('/promo/{slug}', 'PageController@promoDetail')->name('promo.detail');

Route::get('/faq', 'FaqController@index')->name('faq');

Route::post('/simulation', 'FormController@creditSimulation')->name('simulation.post');

Route::post('/subscribe', 'FormController@subscribe')->name('subscribe.post');


Route::get('/term-and-conditions', 'PageController@termandcondition')->name('term_and_conditions');

Route::get('/privacy-policy', 'PageController@privacypolicy')->name('privacy_policy');

Route::get('/email_verification', 'PageController@verification')->name('verification');

Route::post('/testApiPost', 'FormController@testApiPost')->name('testApiPost');
