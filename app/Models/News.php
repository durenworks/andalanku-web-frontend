<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class News extends Model
{
    protected $table = 'news';
    protected $primaryKey = 'id_news';

    public function getImageAttribute($value){
        $baseUrl = Config::get('app.base_image_url');
        return $value ? $baseUrl . 'user_files/news_image/' . $value : '';
    }
}
