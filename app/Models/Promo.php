<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Promo extends Model
{
    protected $table = 'promos';
    protected $primaryKey = 'id_promo';

    public function getImageAttribute($value){
        $baseUrl = Config::get('app.base_image_url');
        return $value ? $baseUrl . 'user_files/promo_image/' . $value : '';
    }
}
