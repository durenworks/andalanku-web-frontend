<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';
    protected $primaryKey = 'id_setting';

    public function scopeSocialMedia($query){
        return $query->where('setting_key', 'Facebook')
            ->orWhere('setting_key', 'Twitter')
            ->orWhere('setting_key', 'Instagram')
            ->orWhere('setting_key', 'youtube');
    }
}
