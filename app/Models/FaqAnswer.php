<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqAnswer extends Model
{
    protected $table = 'faq_answers';
    protected $primaryKey = 'id_answer';

    public function scopeActive($query){
      return $query->where('is_active', "TRUE");
    }

    public function faq(){
      return $this->belongsTo('App\Models\Faq', 'id_faq');
    }
}
