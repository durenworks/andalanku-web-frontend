<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class LegalDocument extends Model
{
    protected $table = 'legal_documents';
    protected $primaryKey = 'id_legal_document';

    public function promo() 
    {
        return $this->belongsTo('App\Models\Promo', 'id_promo');
    }

    public function scopeActive($query)
    {
        return $query->where('is_published', 1);
    }

    public function scopeTos($query){
      return $query->where('type', 'TOS');
    }

    public function scopePrivacyPolicy($query){
      return $query->where('type', 'Privacy Policy');
    }
}
