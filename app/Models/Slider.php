<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Slider extends Model
{
    protected $table = 'sliders';
    protected $primaryKey = 'id_slider';

    public function promo() 
    {
        return $this->belongsTo('App\Models\Promo', 'id_promo');
    }

    public function getImageAttribute($value)
    {
        $baseUrl = Config::get('app.base_image_url');
        return $value ? $baseUrl . 'user_files/slider_image/' . $value : '';
    }

    public function getImageWebAttribute($value)
    {
        $baseUrl = Config::get('app.base_image_url');
        return $value ? $baseUrl . 'user_files/slider_image/' . $value : '';
    }

    public function scopeActive($query)
    {
        return $query->where('id_promo', '!=', 0);
    }
}
