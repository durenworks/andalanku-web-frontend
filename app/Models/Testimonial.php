<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Testimonial extends Model
{
    protected $table = 'testimonials';
    protected $primaryKey = 'id_testimonial';

    public function getImageAttribute($value){
        $baseUrl = Config::get('app.base_image_url');
        return $value ? $baseUrl . 'user_files/testimonial_image/' . $value : '';
    }
}
