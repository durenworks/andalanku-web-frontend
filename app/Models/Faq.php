<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table = 'faq';
    protected $primaryKey = 'id_faq';

    public function answers(){
      return $this->hasMany('App\Models\FaqAnswer', 'id_faq');
    }

    public function scopeActive($query){
      return $query->where('is_active', "TRUE");
    }
}
