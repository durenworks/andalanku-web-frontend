<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Partner extends Model
{
    protected $table = 'partners';
    protected $primaryKey = 'id_partner';

    public function getImageAttribute($value){
        $baseUrl = Config::get('app.base_image_url');
        return $baseUrl . 'user_files/partner_image/' . $value;
    }
}
