<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $api_token = getToken();

        session()->forget([
            'api_token',
            'facebook_url',
            'call_center',
            'instagram2_url',
            'youtube2_url',
            'email_info',
            'whatsapp_number'
        ]);

        session(['api_token' => $api_token]);

        //dd(session()->all());
        /*$data_string = http_build_query(
            array(
                'setting_key' => 'facebook'
            )
        );

        $result = consumeApi('/get_application_setting', $api_token, $data_string);
        //var_dump($result);die();
        $result = json_decode($result);
        $facebook_url = $result->setting_value;

        session(['facebook_url' => $facebook_url]);

        
        $data_string = http_build_query(
            array(
                'setting_key' => 'call center'
            )
        );

        $result = consumeApi('/get_application_setting', $api_token, $data_string);
        $result = json_decode($result);
        $call_center = $result->setting_value;

        session()->put('call_center', $call_center);

        $data_string = http_build_query(
            array(
                'setting_key' => 'instagram'
            )
        );

        $result = consumeApi('/get_application_setting', $api_token, $data_string);
        $result = json_decode($result);

        $instagram_url = $result->setting_value;

        session(['instagram_url' => $instagram_url]);

       $data_string = http_build_query(
           array(
               'setting_key' => 'twitter'
           )
       );

       $result = consumeApi('/get_application_setting', $api_token, $data_string);
       $result = json_decode($result);
       $twitter_url = $result->setting_value;

      // var_dump($result);
    //    /die();
        $data_string = http_build_query(
            array(
                'setting_key' => 'youtube'
            )
        );

        session(['twitter_url' => $twitter_url]);

        $result = consumeApi('/get_application_setting', $api_token, $data_string);
        $result = json_decode($result);
        $youtube_url = $result->setting_value;

        session(['youtube_url' => $youtube_url]);

        $data_string = http_build_query(
            array(
                'setting_key' => 'email info'
            )
        );

        $result = consumeApi('/get_application_setting', $api_token, $data_string);
        $result = json_decode($result);
        $email_info = $result->setting_value;

        session(['email_info' => $email_info]);

        $data_string = http_build_query(
            array(
                'setting_key' => 'whatsapp number'
            )
        );

        $result = consumeApi('/get_application_setting', $api_token, $data_string);
        $result = json_decode($result);
        $whatsapp_number = $result->setting_value;

        session(['whatsapp_number' => $whatsapp_number]);*/


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
