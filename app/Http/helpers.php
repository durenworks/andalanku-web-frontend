<?php
/**
 * Created by PhpStorm.
 * User: Dadittya
 * Date: 18/04/2019
 * Time: 1:02
 */

use App\Models\Partner;
use App\Models\Setting;
use Illuminate\Support\Facades\Config;

if (!function_exists('getToken')) {
    function getToken()
    {
        $key = Config::get('app.api_key');
        $secret = Config::get('app.api_secret');
        $url = Config::get('app.api_url') . '/get_token';

        /*$data_string = http_build_query(
            array(
                'api_key' => $key,
                'api_secret' => $secret
            )
        );*/

        $data_string = [
                'api_key' => $key,
                'api_secret' => $secret
        ];

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        

        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        $result = curl_exec($ch);
        $result = json_decode($result);

        // return $result->token_number;
        return $result;
    }
}
if (!function_exists('consumeApi'))
{
    function consumeApi($endpoint, $token, $data) {
        $key = Config::get('app.api_key');
        $url = Config::get('app.api_url');

        $url = $url . $endpoint;

        /*$result = file_get_contents($url, null, stream_context_create(array(
            'http' => array(
                'method' => 'POST',!@
                'header' => 'Content-type: application/x-www-form-urlencoded' . "\r\n"
                    . 'Content-Length: ' . strlen($data) . "\r\n"
                    . 'api_key: ' . $key . "\r\n"
                    . 'token_number: ' . $token . "\r\n",
                'content' => $data,
            ),
        )));*/

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: application/x-www-form-urlencoded',
            'api_key: ' . $key,
            'token_number: ' . $token)
        );
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        $result = curl_exec($ch);
        $result = json_decode($result);
        
        return $result->token_number;
    }
}

if (!function_exists('getPartner')) {
    function getPartner() {
        $partners = Partner::get();

        return $partners;
    }
}

if (!function_exists('getSocialMedia')) {
    function getSocialMedia() {
        $social_medias = Setting::socialMedia()->get();

        return $social_medias;
    }
}
