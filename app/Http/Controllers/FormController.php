<?php

namespace App\Http\Controllers;

use App\Http\Requests\SimulationRequest;
use App\Http\Requests\SubscribeRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class FormController extends Controller
{

    public $api_token = '';
    public $api_key = '';
    public $api_url = '';

    function __construct()
    {
        $this->api_token = session('api_token');
        $this->api_key = Config::get('app.api_key');
        $this->api_url = Config::get('app.api_url');
    }

    public function creditSimulation(Request $request)
    {
        $data_string = http_build_query(
            array(
                //'api_key' => $this->api_key,
                //'token_number' => $this->api_token,
                'type' => $request->type,
                'otr' => str_replace('.', '', $request->plafond),
                'dp' => $request->dp_percent,
                'tenor' => $request->tenor,
                'asuransi' => $request->insurance,
                'wilayah' => $request->region
            )
        );

        //var_dump($data_string);die();

        $result = consumeApi('/credit_simulation', $this->api_token, $data_string);

        $result = json_decode($result);
        $result = $result->simulation_data[0];

        //var_dump($data_string);
        //var_dump($result->simulation_data[0]);die();
        $form_data = new \stdClass();
        $form_data->plafond = intval(str_replace('.', '', $request->plafond));
        $form_data->dp_percent = $request->dp_percent;
        $form_data->dp_rupiah = intval(str_replace('.', '', $request->dp_rupiah));
        $form_data->tenor = $request->tenor;
        $form_data->tenor_slide = $request->tenor_slide;
        $form_data->region = $request->region;
        $form_data->insurance = $request->insurance;

        // var_dump($form_data);
        // die;

        return redirect()->route('index')
            ->with('simulation_result', $result)
            ->with('simulation', true)
            ->with('type', $request->type)
            ->with('form_data', $form_data);
    }

    public function subscribe(Request $request)
    {
        //var_dump('foo');die();
        $email = $request->email;
        $phone = $request->phone;
        $name = $request->name;

        $data_string = http_build_query(
            array(
                //'api_key' => $this->api_key,
                //'token_number' => $this->api_token,
                'name' => $name == null ? '' : $name,
                'email' => $email == null ? '' : $email,
                'phone' => $phone == null ? '' : $phone,
                'product' => $request->product,
				'otr' => str_replace('.', '', $request->otr),
				'plafond' => str_replace('.', '', $request->plafond),
                'dp_percent' => $request->dp_percent,
				'dp_amount' => str_replace('.', '', $request->dp_amount),
                'tenor' => $request->tenor,
                'insurance' => $request->insurance,
                'region' => $request->region
            )
        );

		//echo "<pre>" . $data_string . "</pre>";

		if ($request->product != '') {
			$sender = 'application';
		}
		else {
			$sender = 'subscribe';
		}
        //var_dump($data_string);die();
        $result = consumeApi('/add_subscriber', $this->api_token, $data_string);

        //var_dump($result);die();
        return redirect()->route('index')
            ->with('subscribed', 'true')
			->with('sender', $sender);
    }

    public function testApiPost(Request $request)
    {
      $out = [ "status" => "cors lewat" ];
      return response()->json($out);
    }
}
