<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use PHPUnit\Util\Json;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{

  function __construct()
  {
  }

  public function index()
  {
    $ip = request()->getClientIp();
    $location = geoip($ip);
    $latitude = $location->lat;
    $longitude = $location->lon;

    $R = 3960;  // earth's mean radius
		$rad = '100';
		// first-cut bounding box (in degrees)
		$maxLat = $latitude + rad2deg($rad/$R);
		$minLat = $latitude - rad2deg($rad/$R);
		// compensate for degrees longitude getting smaller with increasing latitude
		$maxLon = $longitude + rad2deg($rad/$R/cos(deg2rad($latitude)));
		$minLon = $longitude - rad2deg($rad/$R/cos(deg2rad($latitude)));

		$maxLat=number_format((float)$maxLat, 6, '.', '');
		$minLat=number_format((float)$minLat, 6, '.', '');
		$maxLon=number_format((float)$maxLon, 6, '.', '');
    $minLon=number_format((float)$minLon, 6, '.', '');
    
    $query = "SELECT * FROM branches 
    where longitude is not null or latitude is not null 
    ORDER BY (POW((longitude-($longitude)),2) + POW((latitude-($latitude)),2))";

    $branches = DB::select($query);

    return view('landing_page.contact')
      ->with('branches', $branches);
  }

  public function getBranch(Request $request){
    $latitude = $request->lat;
    $longitude = $request->lon;

    $R = 3960;  // earth's mean radius
		$rad = '100';
		// first-cut bounding box (in degrees)
		$maxLat = $latitude + rad2deg($rad/$R);
		$minLat = $latitude - rad2deg($rad/$R);
		// compensate for degrees longitude getting smaller with increasing latitude
		$maxLon = $longitude + rad2deg($rad/$R/cos(deg2rad($latitude)));
		$minLon = $longitude - rad2deg($rad/$R/cos(deg2rad($latitude)));

		$maxLat=number_format((float)$maxLat, 6, '.', '');
		$minLat=number_format((float)$minLat, 6, '.', '');
		$maxLon=number_format((float)$maxLon, 6, '.', '');
    $minLon=number_format((float)$minLon, 6, '.', '');
    
    $query = "SELECT * FROM branches 
    WHERE longitude BETWEEN '$minLon' AND '$maxLon' 
    AND latitude BETWEEN '$minLat' AND '$maxLat'
    ORDER BY (POW((longitude-($longitude)),2) + POW((latitude-($latitude)),2))";

    $branches = DB::select($query);

    return response()->json($branches);
  }
}
