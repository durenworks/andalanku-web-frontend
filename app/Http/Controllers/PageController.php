<?php

namespace App\Http\Controllers;

use App\Models\LegalDocument;
use App\Models\News;
use App\Models\Promo;
use App\Models\Slider;
use App\Models\Testimonial;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use League\Flysystem\Plugin\ForcedRename;
use PHPUnit\Util\Json;
use Illuminate\Support\Carbon;
use stdClass;

class PageController extends Controller
{
    public $api_token = '';
    public $api_key = '';
    public $api_url = '';

    function __construct()
    {
        $this->api_token = session('api_token');
        $this->api_key = Config::get('app.api_key');
        $this->api_url = Config::get('app.api_url');
    }

    public function index()
    {
        $getSliders = Slider::orderBy('id_slider')
                    ->get();
		$sliders = [];
		$now = Carbon::now();
		foreach($getSliders as $value){
            $data = new stdClass();
            $data->id_slider = $value->id_slider;
            $data->image = $value->image;
            $data->image_web = $value->image_web;
            $data->id_promo = $value->id_promo;
            $data->promo_title = '';
            $data->promo_slug = '';
			$activePromo = $value
				->promo()
				->where('promo_start_date', '<', $now)
				->where('promo_expired_date', '>', $now)
				->first();
			if($activePromo){
                $data->promo_title = $activePromo->title;
                $data->promo_slug = $activePromo->slug;
				$sliders[] = $data;
			}else{
                $sliders[] = $data;
            }
        }

        $testimonials = Testimonial::get();
        $last_key = $testimonials->keys()->last();
        $testimonials_result = [];
        $items = [];
        foreach($testimonials as $key => $testimonial){
            $testi = new stdClass();
            $testi->id_testimonial = $testimonial->id_testimonial;
            $testi->name = $testimonial->name;
            $testi->testimonial = $testimonial->testimonial;
            $testi->profession = $testimonial->profession;
            $testi->image = $testimonial->image;
            $items[] = $testi;
            if(($key + 1 ) % 3 == 0 || $key == $last_key){
                $testimonials_result[] = $items;
                $items = [];
            }
        }

		return view('landing_page.index')
			->with('sliders', $sliders)
            ->with('testimonials_result', $testimonials_result);
    }

    public function newsPage()
    {
        $news_lists = News::all();

        return view('landing_page.news')
            ->with('result', $news_lists);
    }

    public function promoPage()
    {
        $now = Carbon::now();
        $promo_lists = Promo::where('promo_start_date', '<', $now)
            ->where('promo_expired_date', '>', $now)->limit(3)->get();

        return view('landing_page.promo')
            ->with('result', $promo_lists);
    }

    public function sortFunction($a, $b)
    {
        return strtotime($a["date"]) - strtotime($b["date"]);
    }

    public function newsDetail($slug)
    {
        $news_detail = News::where('slug', $slug)->first();
        $news_lists = News::inRandomOrder()->limit(3)->get();

        $page_type = 'berita';

        return view('landing_page.news-detail')
            ->with('item', $news_detail)
            ->with('list', $news_lists)
            ->with('page_type', $page_type)
            ->with('title', $news_detail->title)
            ->with('image', $news_detail->image);
    }

    public function promoDetail($slug)
    {
        $promo_detail = Promo::where('slug', $slug)->first();
        $now = Carbon::now();
        $promo_lists = Promo::where('promo_start_date', '<', $now)
            ->where('promo_expired_date', '>', $now)->limit(3)->get();

        return view('landing_page.news-detail')
            ->with('item', $promo_detail)
            ->with('list', $promo_lists)
            ->with('page_type', 'promo')
            ->with('title', $promo_detail->title)
            ->with('image', $promo_detail->image);
    }

    public function termandcondition(){

        $legal_document = LegalDocument::active()->tos()->first();
        $tos = '';
        if($legal_document){
            $tos = $legal_document->content;
        }
        return view('landing_page.tos')
                ->with('tos', $tos);

    }

    public function privacypolicy(){

        $legal_document = LegalDocument::active()->privacyPolicy()->first();
        $privacy = '';
        if($legal_document){
            $privacy = $legal_document->content;
        }

        return view('landing_page.privacy')
            ->with('privacy', $privacy);

    }

    public function verification() {
        return view('landing_page.verification');
    }
}
