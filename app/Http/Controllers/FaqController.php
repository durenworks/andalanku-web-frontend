<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use PHPUnit\Util\Json;

class FaqController extends Controller
{
  public $api_token = '';
  public $api_key = '';
  public $api_url = '';

  function __construct()
  {
    $this->api_token = session('api_token');
    $this->api_key = Config::get('app.api_key');
    $this->api_url = Config::get('app.api_url');
  }

  public function index()
  {
    $lists = Faq::active()->with(['answers' => function($query){
      return $query->active();
    }])->get();

    return view('landing_page.faq')
      ->with('faq_results', $lists);
  }
}
