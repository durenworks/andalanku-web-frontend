
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app'
// });

window.submitForm = function(formName) {
  $('.ld').addClass('active');
  setTimeout(() => {
    $(`${formName}`).submit();
  }, 2000);
}

// DOCUMENTATION : http://instafeedjs.com/
//var Instafeed = require("instafeed.js");
//var feed = new Instafeed({
//  get: 'popular',
//  clientId: '643668826419397',
//      accessToken: 'IGQVJXcmxTd3Q0bzBwRE9KdjlmODR2dU94Ym00V2J4eDc5c1BWZADQ2cVZAYNzQ1Y00xaFg1emhNSktoTDlFenE0LWpjTGRIcC1OQWptOWVDOG42MDJ2aDN4ZATFHc2tCUV9mMXVKZAGY4M0dvN0trc2hQNAZDZD',
//      userId: 'andalanku_id',
//});
//feed.run();