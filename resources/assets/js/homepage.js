/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/homepage.js":
/***/ (function(module, exports) {

/******/;(function (modules) {
  // webpackBootstrap
  /******/ // The module cache
  /******/var installedModules = {}; // The require function
  /******/
  /******/ /******/function __webpack_require__(moduleId) {
    /******/
    /******/ // Check if module is in cache
    /******/if (installedModules[moduleId]) {
      /******/return installedModules[moduleId].exports;
      /******/
    } // Create a new module (and put it into the cache)
    /******/ /******/var module = installedModules[moduleId] = {
      /******/i: moduleId,
      /******/l: false,
      /******/exports: {}
      /******/
    }; // Execute the module function
    /******/
    /******/ /******/modules[moduleId].call(module.exports, module, module.exports, __webpack_require__); // Flag the module as loaded
    /******/
    /******/ /******/module.l = true; // Return the exports of the module
    /******/
    /******/ /******/return module.exports;
    /******/
  } // expose the modules object (__webpack_modules__)
  /******/
  /******/
  /******/ /******/__webpack_require__.m = modules; // expose the module cache
  /******/
  /******/ /******/__webpack_require__.c = installedModules; // define getter function for harmony exports
  /******/
  /******/ /******/__webpack_require__.d = function (exports, name, getter) {
    /******/if (!__webpack_require__.o(exports, name)) {
      /******/Object.defineProperty(exports, name, {
        /******/configurable: false,
        /******/enumerable: true,
        /******/get: getter
        /******/
      });
      /******/
    }
    /******/
  }; // getDefaultExport function for compatibility with non-harmony modules
  /******/
  /******/ /******/__webpack_require__.n = function (module) {
    /******/var getter = module && module.__esModule ? /******/function getDefault() {
      return module['default'];
    } : /******/function getModuleExports() {
      return module;
    };
    /******/__webpack_require__.d(getter, 'a', getter);
    /******/return getter;
    /******/
  }; // Object.prototype.hasOwnProperty.call
  /******/
  /******/ /******/__webpack_require__.o = function (object, property) {
    return Object.prototype.hasOwnProperty.call(object, property);
  }; // __webpack_public_path__
  /******/
  /******/ /******/__webpack_require__.p = ''; // Load entry module and return exports
  /******/
  /******/ /******/return __webpack_require__(__webpack_require__.s = 1);
  /******/
})(
/************************************************************************/
/******/{
  /***/'./resources/assets/js/footer.js': /***/function resourcesAssetsJsFooterJs(module, exports) {
    var validateFooterForm = $('#footerForm').validate({
      rules: {
        name: {
          required: true
        },
        email: {
          required: true,
          email: true
        },
        phone: {
          required: true
        }
      },
      messages: {
        name: {
          required: 'Nama tidak boleh kosong'
        },
        email: {
          required: 'Email tidak boleh kosong',
          email: 'format email salah'
        },
        phone: {
          required: 'Nomor telepone tidak boleh kosong'
        }
      }
    });

    /***/
  },

  /***/'./resources/assets/js/homepage.js': /***/function resourcesAssetsJsHomepageJs(module, exports, __webpack_require__) {
    // Properties

    var plafondProperties = {
      mask: Number,
      scale: 2,
      signed: false,
      thousandsSeparator: '.',
      padFractionalZeros: false,
      normalizeZeros: true,
      radix: ',',
      mapToRadix: ['.'],
      min: 50000000,
      max: 1000000000
    };

    var dpRupiahProperties = {
      mask: Number,
      scale: 2,
      signed: false,
      thousandsSeparator: '.',
      padFractionalZeros: false,
      normalizeZeros: true,
      radix: ',',
      mapToRadix: ['.']
    };

    var dpPercentProperties = {
      mask: Number,
      signed: false,
      max: 100

      /**
       *  ==================================
       *              New Cars
       *  ==================================
       *
       *  rules menggunakan input name
       */
    };
    var validateNewCars = $('#newcarsForm').validate({
      rules: {
        plafond: {
          required: true
        },
        dp_percent: {
          required: true,
          min: 20,
          max: 100
        },
        dp_rupiah: {
          required: true
        },
        tenor: {
          required: true
        },
        insurance: {
          required: true
        },
        region: {
          required: true
        }
      },
      messages: {
        plafond: {
          required: 'Harga mobil tidak boleh kosong'
        },
        dp_percent: {
          required: 'Uang muka tidak boleh kosong',
          min: 'Minimal uang muka 20%'
        },
        dp_rupiah: {
          required: 'Uang muka tidak boleh kosong'
        },
        tenor: {
          required: 'Silakan pilih jangka waktu'
        },
        insurance: {
          required: 'Silakan pilih asuransi'
        },
        region: {
          required: 'Silakan pilih wilayah Anda'
        }
      }
    });

    var validateNewCarsSubmitRequest = $('#newcarsSubmitRequest').validate({
      rules: {
        name: {
          required: true
        },
        email: {
          required: true
        },
        phone: {
          required: true
        }
      },
      messages: {
        name: {
          required: 'silakan isi nama Anda'
        },
        email: {
          required: 'silakan isi email Anda'
        },
        phone: {
          required: 'silakan isi nomor telefon Anda'
        }
      }
    });

    // Input Mask

    $('#newcars_plafond_slider').on('input', function () {
      newcars_plafond.typedValue = $(this).val();
    });

    $('#newcars_dp_slider').on('input', function () {
      newcars_dp_percent.typedValue = $(this).val();
    });

    var newcars_plafond = IMask(document.getElementById('newcars_plafond'), plafondProperties).on('accept', function () {
      validateNewCars.element('#newcars_plafond');
      var val = document.getElementById('newcars_plafond').value;
      val = val.split('.').join('');
      document.getElementById('newcars_plafond_slider').value = parseInt(val);
      // newcars_dp_percent.typedValue = '';
      // newcars_dp_rupiah.typedValue = '';
      // document.getElementById('newcars_dp_slider').value = 0;
      if (newcars_dp_percent.masked.number >= 20) {
        dp_cash = parseInt(val) * newcars_dp_percent.masked.number / 100;
        newcars_dp_rupiah.typedValue = dp_cash;
      }
    });

    var newcars_dp_rupiah = IMask(document.getElementById('newcars_dp_rupiah'), dpRupiahProperties).on('accept', function () {
      validateNewCars.element('#newcars_plafond');
      validateNewCars.element('#newcars_dp_percent');
      validateNewCars.element('#newcars_dp_rupiah');

      if (newcars_plafond.masked.number >= 50000000) {
        percent = newcars_dp_rupiah.masked.number * 100 / newcars_plafond.masked.number;
        newcars_dp_percent.typedValue = Math.ceil(percent);
      }
    });

    var newcars_dp_percent = IMask(document.getElementById('newcars_dp_percent'), dpPercentProperties).on('accept', function () {
      validateNewCars.element('#newcars_plafond');
      validateNewCars.element('#newcars_dp_percent');
      validateNewCars.element('#newcars_dp_rupiah');
      var val = document.getElementById('newcars_dp_percent').value;
      document.getElementById('newcars_dp_slider').value = parseInt(val);
      if (newcars_plafond.masked.number >= 50000000 && newcars_dp_percent.masked.number >= 20) {
        dp_cash = newcars_plafond.masked.number * newcars_dp_percent.masked.number / 100;
        newcars_dp_rupiah.typedValue = dp_cash;
        //alert(dp_cash);
      }
    });

    /**
     *  ==================================
     *              Used Cars
     *  ==================================
     *
     *  rules menggunakan input name
     */

    var validateUsedCars = $('#usedcarsForm').validate({
      rules: {
        plafond: {
          required: true
        },
        dp_percent: {
          required: true,
          min: 20,
          max: 100
        },
        dp_rupiah: {
          required: true
        },
        tenor: {
          required: true
        },
        insurance: {
          required: true
        },
        region: {
          required: true
        }
      },
      messages: {
        plafond: {
          required: 'Harga mobil tidak boleh kosong'
        },
        dp_percent: {
          required: 'Uang muka tidak boleh kosong',
          min: 'Minimal uang muka 20%'
        },
        dp_rupiah: {
          required: 'Uang muka tidak boleh kosong'
        },
        tenor: {
          required: 'Silakan pilih jangka waktu'
        },
        insurance: {
          required: 'Silakan pilih asuransi'
        },
        region: {
          required: 'Silakan pilih wilayah Anda'
        }
      }
    });

    // Input Mask
    $('#usedcars_plafond_slider').on('input', function () {
      usedcars_plafond.typedValue = $(this).val();
    });

    $('#usedcars_dp_slider').on('input', function () {
      usedcars_dp_percent.typedValue = $(this).val();
    });

    var usedcars_plafond = IMask(document.getElementById('usedcars_plafond'), plafondProperties).on('accept', function () {
      validateUsedCars.element('#usedcars_plafond');
      var val = document.getElementById('usedcars_plafond').value;
      val = val.split('.').join('');
      document.getElementById('usedcars_plafond_slider').value = parseInt(val);
      if (usedcars_dp_percent.masked.number >= 20) {
        dp_cash = parseInt(val) * usedcars_dp_percent.masked.number / 100;
        usedcars_dp_rupiah.typedValue = dp_cash;
      }
    });

    var usedcars_dp_rupiah = IMask(document.getElementById('usedcars_dp_rupiah'), dpRupiahProperties).on('accept', function () {
      validateUsedCars.element('#usedcars_plafond');
      validateUsedCars.element('#usedcars_dp_percent');
      validateUsedCars.element('#usedcars_dp_rupiah');

      if (usedcars_plafond.masked.number >= 50000000) {
        percent = usedcars_dp_rupiah.masked.number * 100 / usedcars_plafond.masked.number;
        usedcars_dp_percent.typedValue = Math.ceil(percent);
      }
    });

    var usedcars_dp_percent = IMask(document.getElementById('usedcars_dp_percent'), dpPercentProperties).on('accept', function () {
      validateUsedCars.element('#usedcars_plafond');
      validateUsedCars.element('#usedcars_dp_percent');
      validateUsedCars.element('#usedcars_dp_rupiah');
      var val = document.getElementById('usedcars_dp_percent').value;
      document.getElementById('usedcars_dp_slider').value = parseInt(val);
      if (usedcars_plafond.masked.number >= 50000000 && usedcars_dp_percent.masked.number >= 20) {
        dp_cash = usedcars_plafond.masked.number * usedcars_dp_percent.masked.number / 100;
        usedcars_dp_rupiah.typedValue = dp_cash;
      }
    });

    /**
     *  ==================================
     *              Dana Form
     *  ==================================
     */

    var validateDana = $('#danaForm').validate({
      rules: {
        plafond: {
          required: true
        },
        tenor: {
          required: true
        },
        insurance: {
          required: true
        },
        region: {
          required: true
        }
      },
      messages: {
        plafond: {
          required: 'Jumlah plafond tidak boleh kosong'
        },
        tenor: {
          required: 'Silakan pilih jangka waktu'
        },
        insurance: {
          required: 'Silakan pilih asuransi'
        },
        region: {
          required: 'Silakan pilih wilayah Anda'
        }
      }
    });

    var dana_plafond = IMask(document.getElementById('dana_plafond'), plafondProperties).on('accept', function () {
      validateUsedCars.element('#dana_plafond');
    });

    __webpack_require__('./resources/assets/js/footer.js');

    /***/
  },

  /***/1: /***/function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__('./resources/assets/js/homepage.js');

    /***/
  }

  /******/
});

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/js/homepage.js");


/***/ })

/******/ });