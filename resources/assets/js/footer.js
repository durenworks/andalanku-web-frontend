var validateFooterForm = $('#footerForm').validate({
  rules: {
    name: {
      required: true
    },
    email: {
      required: true,
      email: true
    },
    phone: {
      required: true,
    },
  },
  messages: {
    name: {
      required: 'Nama tidak boleh kosong'
    },
    email: {
      required: 'Email tidak boleh kosong',
      email: 'format email salah'
    },
    phone: {
      required: 'Nomor telepone tidak boleh kosong'
    }
  }
});