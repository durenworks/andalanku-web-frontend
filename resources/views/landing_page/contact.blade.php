@extends('landing_page.template')
@section('content')
<header id="header" class="bg-lightblue is-white header-contact pt-md-5">
  <div class="container">
    <div class="text-center py-5">
      <h1 class="heading-text size-1 is-bold pb-5 is-brownish-grey">GET IN TOUCH</h1>
    </div>
  </div>
</header>

<section id="intro" style="margin-top: -150px">
  <div class="container">
    <div class="row px-3">
      <div class="card">
        <div class="card-body rounded-0 p-5 bg-white shadow">
          <h4 class="card-title is-azure size-2 is-bold mb-5 text-center">HEAD OFFICE</h4>
          <p class="card-text px-md-5 mb-5 mx-md-5">
            Jalan Sunburst CBD Lot II No. 3, BSD City, Lengkong Gudang, Serpong, Lengkong Gudang, Serpong, Kota Tangerang Selatan, Banten 15321 <br><br>
            <i class="fas fa-phone fa-fw"></i> (021) 22356888 <br><br>
            <i class="fas fa-envelope-open-text fa-fw"></i> support@andalanfinance.com
          </p>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="contact" class="pt-5">
  <div class="container mt-5">
    <h4 class="header-title is-azure size-2 mb-5 pb-5 text-center has-line">BRANCH OFFICE</h4>
    <div class="row" id="branch">
      @forelse ($branches as $branch)
        <div class="col-sm-6 mb-md-5 mb-sm-3">
          <h6 class="heading-text is-bold">{{ $branch->name }}</h6>
          <p>
            {{ $branch->address }} <br>
            {{-- {{ $branch->city }} {{ $branch->zipcode }} <br> --}}
            Telp: {{ $branch->phone }} <br>
            Fax: {{ $branch->fax }}
          </p>
        </div>
      @empty
        <div class="col-sm-6 mb-md-5 mb-sm-3">
          <h6 class="heading-text is-bold">PEMUDA (Jakarta Timur 1)</h6>
          <p>
            Ruko Jalan Pahlawan Revolusi No. 145 Blok B-D, <br>
            Pondok Bambu, Jakarta Timur 13430 <br>
            Telp: (021) 2957 4855 <br>
            Fax: (021) 2957 4854
          </p>
        </div>
        <div class="col-sm-6 mb-md-5 mb-sm-3">
          <h6 class="heading-text is-bold">PONDOK INDAH (Jakarta Selatan)</h6>
          <p>
            Jl. Coputat Raya No. 31, Pondok Pinang <br>
            Jakarta Selatan 12310 <br>
            Telp: (021) 2923 5511 <br>
            Fax: (021) 2923 5366
          </p>
        </div>
        <div class="col-sm-6 mb-md-5 mb-sm-3">
          <h6 class="heading-text is-bold">WTC (Jakarta Utara)</h6>
          <p>
            Jl. Griya Sejahtera, Ancol Selatan Sunter Icon<br>
            Blok D-1 Sunter Agung, Jakarta Utara 14350<br>
            Telp: (021) 2956 1988 <br>
            Fax: (021) 2956 1989
          </p>
        </div>
        <div class="col-sm-6 mb-md-5 mb-sm-3">
          <h6 class="heading-text is-bold">KALIMALANG (Jakarta Timur 2)</h6>
          <p>
            Ruko Graha Mas Pemuda, Jl. Pemuda Raya Blok AD No. 8, <br>
            Jakarta Timur 13220 <br>
            Telp: (021) 2957 5102 <br>
            Fax: (021) 2957 5103
          </p>
        </div>
        <div class="col-sm-6 mb-md-5 mb-sm-3">
          <h6 class="heading-text is-bold">KEBON KEJUK (Jakarta Barat)</h6>
          <p>
            Ruko Business Park Blok I-9 Gd Kencana Tower, <br>
            Jl. Meruya Ilir No. 88, Jakarta Barat 11530 <br>
            Telp: (021) 5890 8283 <br>
            Fax: (021) 5890 8284
          </p>
        </div>
        <div class="col-sm-6 mb-md-5 mb-sm-3">
          <h6 class="heading-text is-bold">BOGOR</h6>
          <p>
            Ruko Bantar Kemang Jl. Raya Pajajaran No. 20 E, Baranangsiang <br>
            Bogor Timur 16143 <br>
            Telp: (021) 834 4100 <br>
            Fax: (021) 834 4155
          </p>
        </div>
      @endforelse
    </div>
  </div>
</section>
@endsection

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
      var options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      };

      function success(pos) {
        var crd = pos.coords;
        $.ajax({
            url: "{{ route('contact.get_branch') }}",
            type: 'GET',
            dataType: 'json', // added data type
            data: {
              lat: crd.latitude,
              lon: crd.longitude
            },
            success: function(res) {
              if(res.length){
                arr = res.map(function(data){
                  return `
                    <div class="col-sm-6 mb-md-5 mb-sm-3">
                      <h6 class="heading-text is-bold">${data.name}</h6>
                      <p>
                        ${data.address} <br>
                        Telp: ${data.phone} <br>
                        Fax: ${data.fax}
                      </p>
                    </div>
                  `
                })
                const branch = `
                  <div class="row">
                    ${arr.join('')}
                  </div>
                `
                $('#branch').replaceWith(branch)
              }
            }
        });
      }

      function error(err) {
        console.warn(`ERROR(${err.code}): ${err.message}`);
      }

      navigator.geolocation.getCurrentPosition(success, error, options);
    })
  </script>
@endsection