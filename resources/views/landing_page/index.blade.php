@extends('landing_page.template')

@section('css')
<link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
<style>
html {
scroll-behavior: smooth;
}
</style>
@endsection

@section('content')
<header id="header" class="bg-lightblue is-white position-relative">
  <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner swiper-slide">
      @forelse($sliders as $key => $slider)
      <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
        <div class="carausel-image">
          @if ($slider->id_promo)
            <a href="{{ route('promo.detail', $slider->promo_slug) }}">
              <img src="{{ $slider->image_web ? $slider->image_web : $slider->image }}" alt="">
            </a>
          @else
            <img src="{{ $slider->image_web ? $slider->image_web : $slider->image }}" alt="">
          @endif
        </div>
      </div>
      @empty
      <div class="carousel-item active">
        <div class="container">
          <div class="swiper-content pull-right pt-5">
            <h2 class="header-title is-extrabold has-text-shadow">Download Aplikasi Andalanku Sekarang!</h2>
            <p class="intro-text size-4">ANDALANKU.ID hadir sebagai aplikasi dengan fitur-fitur unggulan yang menawarkan kemudahan dalam pengajuan kredit kendaraan serta multiguna</p>
            <a href="https://play.google.com/store/apps/details?id=id.andalan.andalanku" target="_blank" class="btn-img"><img src="{{ asset('images/playstore.png') }}" alt=""></a>
          </div>
          <div class="swiper-image">
            <img src="{{ asset('images/slider-bg-1.png') }}" style="max-width:400px;" alt="">
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <div class="container">
          <div class="swiper-content pull-left pt-5">
            <h2 class="header-title is-extrabold mb-5 has-text-shadow">Pinjaman cepat, mudah & banyak pilihan untuk berbagai kebutuhan</h2>
            <a href="#" class="btn btn-secondary btn-lg">Mau Tau?</a>
          </div>
          <div class="swiper-image text-right">
            <img src="{{ asset('images/slider-bg-2.png') }}" style="max-width:700px;transform: translate(12%, 10px);" alt="">
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <div class="container">
          <div class="swiper-content pull-right pt-5">
            <h2 class="header-title is-extrabold mb-5 has-text-shadow">Melalui Aplikasi Pintar kami siap menjadi partner kuangan Anda</h2>
            <a href="#" class="btn btn-secondary btn-lg">Mau Tau?</a>
          </div>
          <div class="swiper-image">
            <img src="{{ asset('images/slider-bg-3.png') }}" style="max-width:500px;" alt="">
          </div>
        </div>
      </div>
      @endforelse
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev" style="z-index: 99;">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next" style="z-index: 99;">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</header>

<section id="simulation" class="py-5 bg-white-two">
  <div class="container">
    <div class="row">
      <div class="col-md-12 pt-5">
        <h2 class="heading-text size-1 is-bold is-azure mb-md-4">ANDALANKU.ID</h2>
        <p class="heading-text size-4 mb-md-5">Melalui aplikasi pintar kami siap menjadi partner <br> keuangan Andalan Anda</p>
      </div>
      <div class="col-md-12">
        <div class="simulation">
          <h4 class="heading-text size-3 is-medium is-azure">SIMULASI PEMBIAYAAN</h4>
          <p class="mt-3 mb-5">Ayo cari tahu nilai angsuran Anda! <br> Pilih produknya, simulasikan angkanya dan ajukan pembiayaan sekarang</p>
            <?php
                $simulation_result = session()->get( 'simulation_result' );
                $simulation = session()->get('simulation');
                $type = session()->get('type');
                $form_data = session()->get('form_data');
                //$type = session()->get( 'type' );
                //echo 'type'. $type;
            ?>

          <div class="simulation__body px-md-5">
            <!-- Simulation Tab Menu -->
            <ul class="nav nav-tabs nav-justified" id="simulation__tab" role="tablist">
              <li class="nav-item">
                <a class="nav-link d-flex align-items-center justify-content-md-center {{ ($type == null || $type == 'NEW') ? 'active' : '' }}" href="#newcars" id="newcars__tab" data-toggle="tab" role="tab" aria-controls="newcars" aria-selected="true"><span class="simulation__icon newcars"></span>&nbsp;&nbsp;&nbsp;&nbsp;NEW CARS</a>
              </li>
              <li class="nav-item">
                <a class="nav-link d-flex align-items-center justify-content-md-center {{ ($type == 'USED') ? 'active' : '' }}" href="#usedcars" id="usedcars__tab" data-toggle="tab" role="tab" aria-controls="usedcars" aria-selected="false"><span class="simulation__icon usedcars"></span>&nbsp;&nbsp;&nbsp;&nbsp;USED CARS</a>
              </li>
              <li class="nav-item">
                <a class="nav-link d-flex align-items-center justify-content-md-center {{ ($type == 'KMG') ? 'active' : '' }}" href="#danaandalanku" id="danaandalanku__tab" data-toggle="tab" role="tab" aria-controls="danaandalanku" aria-selected="false"><span class="simulation__icon danaandalanku"></span>&nbsp;DANA ANDALANKU</a>
              </li>
              {{--<li class="nav-item">--}}
                {{--<a class="nav-link d-flex align-items-center justify-content-md-center" href="#ecommerce" id="ecommerce__tab" data-toggle="tab" role="tab" aria-controls="ecommerce" aria-selected="false"><span class="simulation__icon ecommerce"></span>&nbsp;&nbsp;&nbsp;&nbsp;E-COMMERCE</a>--}}
              {{--</li>--}}
            </ul>
            <!-- End of Simulation Tab Menu -->

            <!-- Simulation Tab Content -->
            <div class="tab-content" id="simulation__content">
              <div class="tab-pane fade py-5 {{ ($type == null || $type == 'NEW') ? 'show active' : '' }}" id="newcars" role="tabpanel" aria-labelledby="newcars__tab">
                @include('landing_page.partials._tab-content-newcars', ['simulation_result' => $simulation_result])
              </div>
              <div class="tab-pane fade py-5 {{ ($type == 'USED') ? 'show active' : '' }}" id="usedcars" role="tabpanel" aria-labelledby="usedcars__tab">
                @include('landing_page.partials._tab-content-usedcars', ['simulation_result' => $simulation_result])
              </div>
              <div class="tab-pane fade py-5 {{ ($type == 'KMG') ? 'show active' : '' }}" id="danaandalanku" role="tabpanel" aria-labelledby="danaandalanku__tab">
                @include('landing_page.partials._tab-content-dana', ['simulation_result' => $simulation_result])
              </div>
              {{--<div class="tab-pane fade py-5" id="ecommerce" role="tabpanel" aria-labelledby="ecommerce__tab">--}}
                {{--@include('landing_page.partials._tab-content-ecommerce')--}}
              {{--</div>--}}
            </div>
            <!-- End of Simulation Tab Content -->

            <div class="d-block">
              <h5 class="mb-3">Download Andalanku.id di Mobile App:</h5>
              <a href="#">
                <img src="{{ asset('images/playstore.png') }}" alt="" class="mw-200">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="services" class="py-5 bg-white-two">
  <div class="container">
    <div class="row pb-5">
      <div class="col-md-12 my-5 text-center">
        <h2 class="heading-text size-3 is-medium is-azure text-center">KELEBIHAN KITA</h2>
        <p class="has-line">Ayo cari tahu nilai angsuran Anda! <br> Pilih produknya, simulasikan angkanya dan ajukan pembiayaan sekarang</p>
      </div>
      <div class="col-md-4 mb-4">
        <div class="text-center">
          <img src="{{ asset('images/icn-rate-bunga.png') }}" alt="" class="mb-3 img-feature">
          <h5>Rate Bunga Kompetitif</h5>
          <p class="mb-0">Rate bunga mulai dari 0.7%* per bulan.</p>
        </div>
      </div>
      <div class="col-md-4 mb-4">
        <div class="text-center">
          <img src="{{ asset('images/icn-pencairan-cepat.png') }}" alt="" class="mb-3 img-feature">
          <h5>Pencairan Cepat</h5>
          <p class="mb-0">Proses pengajuan sampai pencairan hanya dalam 5 jam!</p>
        </div>
      </div>
      <div class="col-md-4 mb-4">
        <div class="text-center">
          <img src="{{ asset('images/icn-pencairan-tinggi.png') }}" alt="" class="mb-3 img-feature">
          <h5>Nilai Pencairan Tinggi</h5>
          <p class="mb-0">Nilai pinjaman sampai dengan 85% nilai jaminan.</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="testimoni" class="py-5">
  <div class="container">
    <h2 class="heading-text size-3 has-line is-medium is-azure text-center">TESTIMONY</h2>
    <div class="testimony-container py-5">
      <div id="testimoniCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          @forelse($testimonials_result as $key => $items)
            <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
              @foreach ($items as $item)
                <div class="swiper-slide testimony">
                  <img src="{{ $item->image }}" alt="" class="d-block mx-auto">
                  <div class="testimony__content">
                    <p>
                      {{ $item->testimonial }}
                    </p>
                    <p>
                      <span class="is-azure is-bold">{{ $item->name }}</span>{{ ', ' . $item->profession  }}
                    </p>
                  </div>
                </div>  
              @endforeach
            </div>
          @empty
            <div class="swiper-slide testimony">
              <img src="{{ asset('images/team1.png') }}" alt="" class="d-block mx-auto">
              <div class="testimony__content">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, feugiat a risus ac,
                </p>
                <p>
                  <span class="is-azure is-bold">Sule Sutisna</span>, Designer
                </p>
              </div>
            </div>
            <div class="swiper-slide testimony">
              <img src="{{ asset('images/team1.png') }}" alt="" class="d-block mx-auto">
              <div class="testimony__content">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, feugiat a risus ac,
                </p>
                <p>
                  <span class="is-azure is-bold">Sule Sutisna</span>, Designer
                </p>
              </div>
            </div>
            <div class="swiper-slide testimony">
              <img src="{{ asset('images/team1.png') }}" alt="" class="d-block mx-auto">
              <div class="testimony__content">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, feugiat a risus ac,
                </p>
                <p>
                  <span class="is-azure is-bold">Sule Sutisna</span>, Designer
                </p>
              </div>
            </div>
            <div class="swiper-slide testimony">
              <img src="{{ asset('images/team1.png') }}" alt="" class="d-block mx-auto">
              <div class="testimony__content">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, feugiat a risus ac,
                </p>
                <p>
                  <span class="is-azure is-bold">Sule Sutisna</span>, Designer
                </p>
              </div>
            </div>
          @endforelse
  
        </div>
      </div>
      {{-- <div class="swiper-wrapper">
        @forelse($testimonials_result as $item)
        <div class="swiper-slide testimony">
          <img src="{{ $item->image }}" alt="" class="d-block mx-auto">
          <div class="testimony__content">
            <p>
              {{ $item->testimonial }}
            </p>
            <p>
              <span class="is-azure is-bold">{{ $item->name }}</span>{{ ', ' . $item->profession  }}
            </p>
          </div>
        </div>  
        @empty
        
        @endforelse
      </div> --}}
      <!-- Add Pagination -->
      <div class="swiper-pagination"></div>
    </div>
  </div>
</section>
@endsection

@section('scripts')
<!-- <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script> -->
<script type="text/javascript" src="{{ asset('js/homepage.js') }}"></script>
<script type="text/javascript">

  // var mySwiper = new Swiper('.swiper-container', {
  //   loop: true,
  //   autoplay: {
  //     delay: 2500,
  //     disableOnInteraction: false,
  //   },
  //   pagination: {
  //     slidesPerView: 'auto',
  //     el: '.swiper-pagination',
  //     clickable: true
  //   }
  // });

  // var myTestimony = new Swiper('.testimony-container', {
  //   slidesPerView: 3,
  //   spaceBetween: 30,
  //   autoplay: {
  //     delay: 2500,
  //     disableOnInteraction: false,
  //   },
  //   pagination: {
  //     el: '.swiper-pagination',
  //     clickable: true
  //   }
  // });

  $(document).ready(function() {
    
    $('.mat-group .form-control').focusout(function() {
      $('.mat-group label').removeClass('active');
    });
    
    $('.mat-group').on('click', function() {
      $(this).find('.form-control').focus();
      $(this).find('label').addClass('active');
    });

    $('#newcars_btn_calc').click(function() {
      var form = $('#newcarsForm');
      if (form.valid()) {
        submitForm('#newcarsForm');
      } 
    });
    $('#usedcars_btn_calc').click(function() {
      var form = $('#usedcarsForm');
      if (form.valid()) {
        submitForm('#usedcarsForm');
      } 
    });
    $('#newcars_submitnow_btn').click(function() {
      var form = $('#danaForm');
      if (form.valid()) {
        submitForm('#danaForm');
      } 
    });
  });
</script>
  @if (isset($simulation) && $simulation)
    <script type="text/javascript">
        $(document).ready(function() {
            let element = document.getElementById("{{ $type . '_'}}content_result");

            element.scrollIntoView();
        });
    </script>
    <script type="text/javascript">
      $('#btn_recount_new').on('click', function(){
        let element = document.getElementById("NEW_content_form");

        element.scrollIntoView();
      })

      $('#btn_recount_used').on('click', function(){
        let element = document.getElementById("USED_content_form");

        element.scrollIntoView();
      })
  </script>
  @endif
  <script type="text/javascript">
    $('#newcars_tenor_slider').on('input', function(){
      const months = [3,6,12,18,24,30,36,48,60];
      const val = $(this).val()
      $('#newcars_tenor_month').html(months[val])
      $('#newcars_tenor_month').css("padding-right","12px")
      $('#newcars_tenor').val(months[val])
    })

    $('#usedcars_tenor_slider').on('input', function(){
      const months = [3,6,12,18,24,30,36,48,60];
      const val = $(this).val()
      $('#usedcars_tenor_month').html(months[val])
      $('#usedcars_tenor_month').css("padding-right","12px")
      $('#usedcars_tenor').val(months[val])
    })
  </script>
@endsection