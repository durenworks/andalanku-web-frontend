@extends('landing_page.template')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/slick/slick.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/slick/slick-theme.css') }}"/>
@endsection

@section('content')
<header id="header" class="bg-lightblue is-white">
  <div class="container">
    <div class="text-center py-4">
      <h1 class="heading-text size-1 is-bold">BERITA TERBARU <br> SEPUTAR ANDALANKU</h1>
      @include('landing_page.partials._news_nav_menu')
    </div>
  </div>
</header>

<section>
  <div class="container">
    <div class="row my-5">
      <div class="col-md-12 mt-5">
        <h2 class="heading-text is-azure my-5">Sed aliquam lacinia est, vulputate aliquet quam.</h2>

        <div class="single-post bg-white-two">
          <div class="single-post__header">
            <img src="{{ asset('images/soon.png') }}" alt="">
          </div>
          <div class="single-post__content p-5">
            <p>
              Nunc interdum elementum maximus. Donec molestie est vitae nisi rhoncus suscipit. Nunc suscipit diam ut mauris vestibulum, quis fringilla tellus laoreet. Suspendisse in sollicitudin neque, a blandit nulla. Quisque finibus volutpat sagittis. Integer tempus arcu ac efficitur tincidunt. Nam vehicula neque ligula, non lacinia lacus convallis sit amet. Vivamus non sapien lorem. <br><br>
              Donec vehicula iaculis elit nec convallis. Praesent vitae nisi sed nunc pellentesque consectetur in ut quam. Maecenas hendrerit pretium nisi non rutrum. Vestibulum ultricies tempus ante, et scelerisque nulla bibendum dapibus. In gravida luctus dolor, eu ullamcorper tellus facilisis quis. Cras luctus porttitor odio, et hendrerit velit luctus quis. Sed facilisis tellus at ornare tempor. Morbi pharetra gravida mauris non aliquam. Suspendisse potenti. <br><br>
              Quisque in vestibulum arcu. Nam iaculis varius faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur non condimentum leo. Nulla vitae maximus leo. Integer malesuada, urna ac rhoncus fermentum, quam dolor iaculis justo, ut ullamcorper turpis neque quis nisl. Etiam interdum, mi vel malesuada eleifend, sapien eros porttitor metus, eu euismod ex neque ac est. Sed sem tellus, iaculis sed egestas vitae, vestibulum congue ex. Duis sagittis, leo a ornare rhoncus, nisl massa eleifend nulla, a cursus nulla enim a metus. Duis in felis ac nulla tincidunt sodales viverra a lorem. Phasellus pellentesque, nisl quis euismod blandit, lorem lectus sagittis mauris, non molestie eros purus pharetra tellus. Ut nisi quam, tincidunt accumsan suscipit quis, dapibus eget purus. Curabitur scelerisque enim vehicula, gravida sem eu, tincidunt risus. Nunc a condimentum mi, vitae congue purus. Fusce vel sem sit amet lectus venenatis varius sed non mi.
            </p>
          </div>
          <div class="single-post__footer text-center p-5">
            <h5 class="is-bright-orange mb-5">BAGIKAN</h5>
            <ul class="social">
              <li><a href="#"><i class="fab fa-2x fa-facebook"></i></a></li>
              <li><a href="#"><i class="fab fa-2x fa-instagram"></i></a></li>
              <li><a href="#"><i class="fab fa-2x fa-twitter"></i></a></li>
            </ul>
          </div>
        </div>
      </div>

      <div class="col-md-12 mt-5">
        <div class="row">
          <div class="col-md-12 text-center py-5">
            <h3 class="heading-text has-line is-azure">BERITA TERBARU</h3>
          </div>
        </div>
        <div class="row slider">
          <div class="col-sm-4">
            <div class="card mb-4">
              <img class="card-img-top" src="{{ asset('images/bigsale.png') }}" alt="">
              <div class="card-body">
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum feugiat aliquam sem sit amet tincidunt. </p>
                <a href="#" class="d-flex justify-content-center">READ MORE</a>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card mb-4">
              <img class="card-img-top" src="{{ asset('images/bigsale.png') }}" alt="">
              <div class="card-body">
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum feugiat aliquam sem sit amet tincidunt. </p>
                <a href="#" class="d-flex justify-content-center">READ MORE</a>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card mb-4">
              <img class="card-img-top" src="{{ asset('images/bigsale.png') }}" alt="">
              <div class="card-body">
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum feugiat aliquam sem sit amet tincidunt. </p>
                <a href="#" class="d-flex justify-content-center">READ MORE</a>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card mb-4">
              <img class="card-img-top" src="{{ asset('images/bigsale.png') }}" alt="">
              <div class="card-body">
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum feugiat aliquam sem sit amet tincidunt. </p>
                <a href="#" class="d-flex justify-content-center">READ MORE</a>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card mb-4">
              <img class="card-img-top" src="{{ asset('images/bigsale.png') }}" alt="">
              <div class="card-body">
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum feugiat aliquam sem sit amet tincidunt. </p>
                <a href="#" class="d-flex justify-content-center">READ MORE</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('plugins/slick/slick.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.slider').slick({
      infinite: true,
      speed: 300,
      slidesToShow: 3
    });
  });
</script>
@endsection