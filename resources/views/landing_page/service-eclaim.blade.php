@extends('landing_page.template')
@section('content')
<header id="header" class="bg-lightblue header-bg-2 is-white">
  <div class="container">
    <div class="text-center py-4">
      <h1 class="heading-text size-1 is-bold my-5">ONE APPLICATION FOR ALL YOUR FINANCIAL NEEDS</h1>
      @include('landing_page.partials._service_nav_menu', ['service' => 'eclaim'])
    </div>
  </div>
</header>

<section id="intro">
  <div class="container">
    <div class="row mt-5">
      <div class="col-sm-12 mt-5 text-center">
        <h2 class="heading-text has-line is-azure">E-CLAIM ASURANSI</h2>
      </div>
    </div>
  </div>
  <div class="container position-relative">
    <div class="intro mb-5">
      <div class="col-xs-12 col-md-7 mt-md-5 intro-media">
        <img src="{{ asset('images/eclaim-intro.png') }}" alt="Ecomplain Image" class="mt-5">
      </div>
      <div class="col-xs-12 col-md-6 intro-desc top-right p-md-5 p-3">
        <p class="intro-text size-2 is-uppercase">Satu aplikasi untuk sarana pelayanan keluhan anda</p>
        <p class="mb-0">Merupakan fitur layanan pelaporan atau klaim asuransi kendaraan di Aplikasi ANDALANKU.ID bagi pelanggan. Kini pengajuan klaim asuransi bisa dilakukan kapanpun di manapun.</p>
      </div>
    </div>
  </div>
</section>

<section id="howto" class="is-brownish-grey">
  <div class="container-fluid position-relative">
    <div class="container">
      <div class="col-md-10 offset-md-1 col-12 py-3 py-md-0">
        <div class="col-sm-12">
          <h2 class="heading-text is-bold size-5 is-azure mt-5">HOW TO USE <br> E-CLAIM ASURANSI</h2>
        </div>
        <div id="carouselId" class="carousel slide mb-5 carousel-fade" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselId" data-slide-to="0" class="active"></li>
            <li data-target="#carouselId" data-slide-to="1"></li>
            <li data-target="#carouselId" data-slide-to="2"></li>
            <li data-target="#carouselId" data-slide-to="3"></li>
          </ol>
          <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
              <div class="col-sm-12">
                <div class="row d-flex align-items-center">
                  <div class="col-sm-7 howto-text">
                    <p class="is-azure size-4 is-bold">Step 1 Temukan <span class="is-greyish-brown">E-Services</span> menu</p>
                    <p class="is-brownish-grey">
                      Pilih menu <span class="is-bold is-greyish-brown">E-Services</span> pada halaman Home 
                      Aplikasi ANDALANKU.ID, kemudian Pilih
                      <span class="is-bold is-greyish-brown">E-Claim Asuransi</span> untuk Pengajuan klaim atas premi asuransi Anda.
                    </p>
                  </div>
                  <div class="col-sm-5 text-center howto-image">
                    <img src="{{ asset('images/eclaim-step-1.png') }}">
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="col-sm-12">
                <div class="row d-flex align-items-center">
                  <div class="col-sm-7 howto-text">
                    <p class="is-azure size-4 is-bold">Step 2 Pilihlah <span class="is-greyish-brown">E-Claim Asuransi</span></p>
                    <p class="is-brownish-grey">
                      Pilih <span class="is-bold is-greyish-brown">E-Claim Asuransi</span> untuk Pengajuan klaim Asuransi atas premi yang anda miliki melalui Aplikasi ANDALANKU.ID.
                    </p>
                  </div>
                  <div class="col-sm-5 text-center howto-image">
                    <img src="{{ asset('images/eclaim-step-2.png') }}">
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="col-sm-12">
                <div class="row d-flex align-items-center">
                  <div class="col-sm-7 howto-text">
                    <p class="is-azure size-4 is-bold">Step 3 Pilihlah <span class="is-greyish-brown">AJUKAN KLAIM</span></p>
                    <p class="is-brownish-grey">
                      Jika Anda ingin menyampaikan keluhan untuk yang 
                      pertama kalinya Pilih button <span class="is-bold is-greyish-brown">AJUKAN KLAIM</span>, 
                      atau bisa juga tekan Button Icon <span class="is-bold is-greyish-brown">“+”</span> pada Navigasi 
                      Bar untuk membuat pengajuan keluhan melalui Aplikasi ANDALANKU.ID.
                    </p>
                  </div>
                  <div class="col-sm-5 text-center howto-image">
                    <img src="{{ asset('images/eclaim-step-3.png') }}">
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="col-sm-12">
                <div class="row d-flex align-items-center">
                  <div class="col-sm-7 howto-text">
                    <p class="is-azure size-4 is-bold">Step 4 ISI LENGKAP <span class="is-greyish-brown">FORM KLAIM ASURANSI</span></p>
                    <p class="is-brownish-grey">
                      Isi dengan lengkap Form Klaim Asuransi beserta lampirkan file gambar / Foto bukti pendukung atas klaim Asuransi yang Anda sampaikan melalui Apliksi ANDALANKU.ID
                    </p>
                  </div>
                  <div class="col-sm-5 text-center howto-image">
                    <img src="{{ asset('images/eclaim-step-4.png') }}">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
            <i class="fas fa-arrow-circle-left fa-2x"></i>
          </a>
          <a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
            <i class="fas fa-arrow-circle-right fa-2x"></i>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection