<nav class="navbar navbar-top navbar-expand-sm navbar-dark fixed-top bg-lightblue" id="navbar-top">
    <div class="container">
      <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
          aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="{{ route('index') }}"><img src="{{ asset('images/andalan-logo.png') }}" alt="" class="logo"></a>
      <div class="collapse navbar-collapse fade" id="collapsibleNavId">
        <ul class="navbar-nav mx-auto mt-2 mt-lg-0">
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('about') }}">About Us</a>
          </li>
          {{--
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Services
            </a>
            <div class="dropdown-menu fade" aria-labelledby="navbarDropdown">
              <div class="container">
                <div class="row align-items-center py-5">
                  <div class="col">
                    <h3 class="heading-menu">OUR SERVICES</h3>
                  </div>
                  <div class="col">
                    <a href="{{ route('ecomplain') }}" class="dropdown-item">
                      E-COMPLAIN
                      <span class="dropdown-item-desc">Customer Service</span>
                    </a>
                  </div>
                  <div class="col">
                    <a href="{{ route('eclaim') }}" class="dropdown-item">
                      E-CLAIM
                      <span class="dropdown-item-desc">Asuransi</span>
                    </a>
                  </div>
                  <div class="col">
                    <a href="{{ route('erequest') }}" class="dropdown-item">
                      E-REQUEST
                      <span class="dropdown-item-desc">Pengambilan Jaminan</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </li>
          --}}
          <li class="nav-item">
            <a class="nav-link" href="{{ route('services') }}">Service & Product</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('news') }}">News & Promo</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('faq') }}">FAQ</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('contact') }}">Contact Us</a>
          </li>
        </ul>
      </div>
      <a id="subscribe" href="javascript:void(0)" class="btn btn-secondary btn-lg" data-toggle="modal" data-target="#joinModal">Gabung</a>
    </div>
  </nav>