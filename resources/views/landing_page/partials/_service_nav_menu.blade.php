<div class="services-nav-menu d-block mt-5 bg-white mx-5">
  <ul class="nav nav-pills nav-fill py-3">
    <li class="nav-item">
      <a class="nav-link {{ $service == 'ecomplain' ? 'active' : '' }}" href="{{ route('ecomplain') }}"><span class="heading-text is-bold d-block">E-CLOMPLAIN</span>Customer Services</a>
    </li>
    <li class="nav-item">
      <a class="nav-link {{ $service == 'eclaim' ? 'active' : '' }}" href="{{ route('eclaim') }}"><span class="heading-text is-bold d-block">E-CLAIM</span>Asuransi</a>
    </li>
    <li class="nav-item">
      <a class="nav-link {{ $service == 'erequest' ? 'active' : '' }}" href="{{ route('erequest') }}"><span class="heading-text is-bold d-block">E-REQUEST</span>Pengambilan Jaminan</a>
    </li>
  </ul>
</div>