<div class="services-nav-menu mt-5 bg-white mx-5">
  <ul class="nav nav-pills nav-fill py-3">
    <li class="nav-item py-3">
      <a class="nav-link {{ $service == 'news' ? 'active' : '' }}" href="{{ route('news') }}">NEWS</a>
    </li>
    <li class="nav-item py-3">
      <a class="nav-link {{ $service == 'promo' ? 'active' : '' }}" href="{{ route('promo') }}">PROMO</a>
    </li>
  </ul>
</div>