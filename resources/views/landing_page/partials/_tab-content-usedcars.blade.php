<div class="row">
  <div class="col-xs-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2">
    <h3 class="heading-text size-4 is-medium is-greyish-brown has-line py-3 mb-5">Kemudahan Pembiayaan Mobil Bekas dengan Bunga terjangkau dan pilihan dari ratusan Partner showroom di seluruh Indonesia.</h3>
    <!-- Tab Content Form -->
    <div class="tab-content__form" id="USED_content_form">
      @php
        $plafond = null;
        $dp_percent = null;
        $dp_rupiah = null;
        $tenor = 3;
        $tenor_slide = 0;
        $region = null;
        $insurance = null;

        if($simulation_result != '' && $type == 'USED'){
          $plafond = $form_data->plafond;
          $dp_percent = $form_data->dp_percent;
          $dp_rupiah = $form_data->dp_rupiah;
          $tenor = $form_data->tenor;
          $tenor_slide = $form_data->tenor_slide;
          $region = $form_data->region;
          $insurance = $form_data->insurance;
        }
      @endphp
      <form action="{{ route('simulation.post') }}" method="post" class="text-left" id="usedcarsForm">
        <div class="form-row">
          <div class="form-group col-sm-12">
            <label for="">HARGA MOBIL OTR</label>
            <div class="input-group input-group-lg input-group-customs">
              <div class="input-group-prepend">
                <span class="input-group-text">Rp</span>
              </div>
              <input type="text" value="{{ $plafond ? $plafond : '' }}" class="form-control" placeholder="Masukan Harga OTR Mobil" id="usedcars_plafond" name="plafond">
            </div>
            <input
              type="range"
              class="form-control-range mt-3"
              value="{{ $plafond ? $plafond : '50000000' }}"
              value="50000000"
              min="50000000"
              max="1000000000"
              id="usedcars_plafond_slider"
            >
          </div>
          <div class="form-group col-sm-4 col-xs-12">
            <label for="">UANG MUKA</label>
            <div class="input-group input-group-lg input-group-customs">
              <input type="number" value="{{ $dp_percent }}" class="form-control" placeholder="Uang Muka" id="usedcars_dp_percent" min="20" step="1" max="100" name="dp_percent">
              <div class="input-group-prepend">
                <span class="input-group-text">%</span>
              </div>
            </div>
          </div>
          <div class="form-group col-sm-8 col-xs-12">
            <label for="">&nbsp;</label>
            <div class="input-group input-group-lg input-group-customs">
              <div class="input-group-prepend">
                <span class="input-group-text">Rp</span>
              </div>
              <input
                type="text"
                value="{{ $dp_rupiah ? $dp_rupiah : '' }}"
                class="form-control"
                placeholder="Masukan Uang Muka"
                id="usedcars_dp_rupiah"
                name="dp_rupiah">
            </div>
          </div>
          <input
            type="range"
            class="form-control-range mb-3"
            value="{{ $dp_percent ? $dp_percent : 0 }}"
            step="1"
            min="20"
            max="100"
            id="usedcars_dp_slider"
          >
          <div class="form-group col-sm-12">
            <label for="">JANGKA WAKTU</label>
            <div class="input-group input-group-lg input-group-customs">
              <div class="input-group-prepend">
                <span class="input-group-text" id="usedcars_tenor_month">{{ $tenor }}</span>
              </div>
              <div class="input-group-prepend">
                <span class="input-group-text">Bulan</span>
              </div>
            </div>
            <input type="hidden" name="tenor" id="usedcars_tenor" value="{{ $tenor }}">
            <input
              type="range"
              class="form-control-range mt-3"
              value="{{ $tenor_slide }}"
              min="0"
              max="9"
              step="1"
              id="usedcars_tenor_slider"
            >
          </div>
          {{--<div class="form-group col-sm-12">--}}
          {{--<label class="d-flex align-items-center">PEMBAYARAN PERTAMA&nbsp;<i class="far fa-question-circle fa-xs" data-toggle="popover" data-content="pembayaran pertama" data-trigger="hover"></i></label>--}}
          {{--<select class="form-control form-control-lg" name="" id="">--}}
          {{--<option>Pilih Cara Pembayaran Pertama</option>--}}
          {{--<option></option>--}}
          {{--<option></option>--}}
          {{--</select>--}}
          {{--</div>--}}
          <div class="form-group col-sm-12">
            <label class="d-flex align-items-center">ASURANSI JAMINAN</label>
            <select
              class="form-control form-control-lg"
              name="insurance"
              id="usedcars_insurance"
              style="{{ $insurance ? 'color: rgb(0,0,0)' : ''}}"
            >
              <option value="" disabled {{ !$insurance ? 'selected' : '' }}>Pilih Asuransi</option>
              <option value="TLO" {{ $insurance == 'TLO' ? 'selected' : '' }}>TLO</option>
              <option value="ARK" {{ $insurance == 'ARK' ? 'selected' : '' }}>All Risk</option>
            </select>
          </div>
          <div class="form-group col-sm-12">
            <label for="">ZONA WILAYAH</label>
            <select
              class="form-control form-control-lg"
              name="region"
              id="usedcars_region"
              style="{{ $region ? 'color: rgb(0,0,0)' : ''}}"
            >
              <option value="" disabled {{ !$region ? 'selected' : '' }}>Pilih Wilayah Domisili</option>
              <option value="1" {{ $region == 1 ? 'selected' : '' }}>Sumatra dan Kepulauannya</option>
              <option value="2" {{ $region == 2 ? 'selected' : '' }}>Jakarta, Banten dan Jawa Barat</option>
              <option value="3" {{ $region == 3 ? 'selected' : '' }}>Lainnya</option>
            </select>
          </div>
            <input type="hidden" name="type" value="USED">
          <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
          <div class="form-group col-sm-12 text-right">
            <button class="btn btn-secondary btn-lg mt-3" id="usedcars_btn_calc">HITUNG</button>
          </div>
        </div>
      </form>
      <ul class="list-unstyled mt-3 text-left size-7">
        <li>*Syarat dan ketentuan berlaku.</li>
        <li>*Simulasi merupakan bentuk perkiraan, bukan bentuk terakhir dari angsuran.</li>
        <li>*Harga dan bunga bisa berbeda sesuai dengan ketentuan per daerah.</li>
      </ul>
    </div>
    <!-- End of Tab Content Form -->

    <!-- Tab Content Result -->
      @if ($simulation_result != '' && $type == 'USED')
          <div class="tab-content__result" id="USED_content_result">
              <div class="mb-4">
                  <label class="d-block text-bold is-azure mb-3">ESTIMASI ANGSURAN BULANAN</label>
                  <span class="d-inline-block bg-white-three py-3 px-4 is-rounded-half size-2 is-bordered">Rp. {{ $simulation_result->angsuran}} / Bulan</span>
              </div>
              <div class="mb-4">
                  <label class="d-block text-bold is-azure mb-3">JANGKA WAKTU</label>
                  <span class="size-2">{{ $simulation_result->tenor }} Bulan</span>
              </div>

              <button class="btn btn-secondary btn-lg" id="btn_recount_used">HITUNG ULANG</button>

              <ul class="list-unstyled mt-5 text-left size-7">
                  <li>*Syarat dan ketentuan berlaku.</li>
                  <li>*Simulasi merupakan bentuk perkiraan, bukan bentuk terakhir dari angsuran.</li>
                  <li>*Harga dan bunga bisa berbeda sesuai dengan ketentuan per daerah.</li>
              </ul>

              <hr class="my-5">

              <h3 class="heading-text size-4 is-medium is-greyish-brown has-line py-3 mb-5">Ajukan Pembiayaan untuk mendapatkan Mobil Impian anda</h3>
              <div class="col-xs-12 col-sm-8 offset-sm-2">
                  <form action="{{ route('subscribe.post') }}"  method="post"  id="newcarsSubmitRequest">
                      <div class="mat-group input-group-lg">
                          <input type="text" class="form-control" name="name" id="newcars_name" placeholder="Type your name">
                      </div>
                      <div class="mat-group input-group-lg">
                          <input type="text" class="form-control" name="email" id="newcars_email" placeholder="Your E-Mail address">
                      </div>
                      <div class="mat-group input-group-lg">
                          <input type="text" class="form-control" name="phone" id="newcars_phone" placeholder="Your Phone Number">
                      </div>
                      <input type="hidden" name="product" value="USED CAR">
					  <input type="hidden" name="otr" value={{$plafond}}>
					  <input type="hidden" name="dp_percent" value={{$dp_percent}}>
					  <input type="hidden" name="dp_amount" value={{$dp_rupiah}}>
					  <input type="hidden" name="tenor" value={{$tenor}}>
					  <input type="hidden" name="insurance" value={{$insurance}}>
					  <input type="hidden" name="region" value={{$region}}>
                      <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                      <button class="btn btn-secondary btn-lg mt-4" id="newcars-submitnow-btn">AJUKAN SEKARANG</button>
                  </form>
              </div>

          </div>
  @endif
    <!-- End of Tab Content Result -->
  </div>
</div>