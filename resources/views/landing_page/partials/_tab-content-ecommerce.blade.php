<div class="row">
  <div class="col-xs-12 col-sm-8 offset-sm-2">
    <h3 class="heading-text size-4 is-medium is-greyish-brown has-line py-3 mb-5">Pembiayaan Melalui Market Place</h3>
    <!-- Tab Content Form -->
    <div class="tab-content__form">
      <form action="#" class="text-left">
        <div class="form-row">
          <div class="form-group col-sm-12">
            <label for="">HARGA PRODUK</label>
            <div class="input-group input-group-lg">
              <div class="input-group-prepend">
                <span class="input-group-text">Rp</span>
              </div>
              <input type="text" class="form-control" placeholder="Masukan Harga OTR Mobil">
            </div>
          </div>
          <div class="form-group col-sm-4 col-xs-12">
            <label for="">UANG MUKA</label>
            <select class="form-control form-control-lg" name="" id="">
              <option>...%</option>
              <option></option>
              <option></option>
            </select>
          </div>
          <div class="form-group col-sm-8 col-xs-12">
            <label for="">&nbsp;</label>
            <div class="input-group input-group-lg">
              <div class="input-group-prepend">
                <span class="input-group-text">Rp</span>
              </div>
              <input type="text" class="form-control" placeholder="Masukan Uang Muka">
            </div>
          </div>
          <div class="form-group col-sm-12">
            <label for="">JANGKA WAKTU</label>
            <select class="form-control form-control-lg" name="" id="">
              <option>Pilih Jangka Waktu</option>
              <option></option>
              <option></option>
            </select>
          </div>
          <div class="form-group col-sm-12">
            <label for="">ZONA WILAYAH</label>
            <select class="form-control form-control-lg" name="" id="">
              <option>Pilih Wilayah Domisili</option>
              <option></option>
              <option></option>
            </select>
          </div>
          <div class="form-group col-sm-12 text-right">
            <button class="btn btn-secondary btn-lg mt-3" type="submit">HITUNG</button>
          </div>
        </div>
      </form>
      <ul class="list-unstyled mt-3 text-left size-7">
        <li>*Syarat dan ketentuan berlaku.</li>
        <li>*Simulasi merupakan bentuk perkiraan, bukan bentuk terakhir dari angsuran.</li>
        <li>*Harga dan bunga bisa berbeda sesuai dengan ketentuan per daerah.</li>
      </ul>
    </div>
    <!-- End of Tab Content Form -->

    <!-- Tab Content Result -->
    <div class="tab-content__result" hidden>
      <div class="mb-4">
        <label class="d-block text-bold is-azure mb-3">ESTIMASI ANGSURAN BULANAN</label>
        <span class="d-inline-block bg-white-three py-3 px-4 is-rounded-half size-2 is-bordered">Rp. 3.324.973 / Bulan</span>
      </div>
      <div class="mb-4">
        <label class="d-block text-bold is-azure mb-3">JANGKA WAKTU</label>
        <span class="size-2">60 Bulan</span>
      </div>
      
      <button class="btn btn-secondary btn-lg">HITUNG ULANG</button>

      <ul class="list-unstyled mt-5 text-left size-7">
        <li>*Syarat dan ketentuan berlaku.</li>
        <li>*Simulasi merupakan bentuk perkiraan, bukan bentuk terakhir dari angsuran.</li>
        <li>*Harga dan bunga bisa berbeda sesuai dengan ketentuan per daerah.</li>
      </ul>

      <hr class="my-5">

      <h3 class="heading-text size-4 is-medium is-greyish-brown has-line py-3 mb-5">Ajukan Pembiayaan untuk mendapatkan Mobil Impian anda</h3>
      <div class="col-xs-12 col-sm-8 offset-sm-2">
        <form action="#" class="mat-form" id="danaForm">
          <div class="mat-group input-group-lg">
            <input type="text" class="form-control" id="">
            <label>Nama Lengkap Sesuai KTP</label>
          </div>
          <div class="mat-group input-group-lg">
            <input type="text" class="form-control">
            <label>No Handphone</label>
          </div>
          <div class="mat-group input-group-lg">
            <input type="text" class="form-control">
            <label>Alamat Domisili</label>
          </div>
          <div class="mat-group input-group-lg">
            <select name="" id="" class="form-control">
              <option value=""></option>
            </select>
            <label>Area Domisili</label>
          </div>
          <div class="mat-group input-group-lg">
            <select name="" id="" class="form-control">
              <option value=""></option>
            </select>
            <label>Cabang Andalan</label>
          </div>
          <div class="mat-group input-group-lg">
            <select name="" id="" class="form-control">
              <option value=""></option>
            </select>
            <label>Pilihan watu disurvey</label>
          </div>
          <div class="mat-group input-group-lg">
            <input type="text" class="form-control">
            <label>Alamat Domisili</label>
          </div>
          <button class="btn btn-secondary btn-lg mt-4">AJUKAN SEKARANG</button>
        </form>
      </div>

    </div>
    <!-- End of Tab Content Result -->
  </div>
</div>