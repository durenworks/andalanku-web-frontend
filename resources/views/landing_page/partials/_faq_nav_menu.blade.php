<div class="services-nav-menu mt-5 bg-white mx-5">
  <ul class="nav nav-pills nav-fill py-3">
    <li class="nav-item py-3">
      <a class="nav-link {{ $service == 'faq' ? 'active' : '' }}" href="{{ route('faq') }}">FAQ</a>
    </li>
    <li class="nav-item py-3">
      <a class="nav-link {{ $service == 'term_and_conditions' ? 'active' : '' }}" href="{{ route('term_and_conditions') }}">TERM & CONDITIONS</a>
    </li>
    <li class="nav-item py-3">
      <a class="nav-link {{ $service == 'privacy_policy' ? 'active' : '' }}" href="{{ route('privacy_policy')}}">PRIVACY POLICY</a>
    </li>
  </ul>
</div>