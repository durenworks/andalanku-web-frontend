<div class="services-nav-menu mt-5 bg-white mx-5">
  <ul class="nav nav-pills nav-fill py-3">
    <li class="nav-item py-3">
      <a class="nav-link {{ $service == 'tos' ? 'active' : '' }}" href="{{ route('tos') }}">TERM & CONDITIONS</a>
    </li>
    <li class="nav-item py-3">
      <a class="nav-link {{ $service == 'privacy' ? 'active' : '' }}" href="{{ route('privacy') }}">PRIVACY POLICY</a>
    </li>
  </ul>
</div>