<section id="newsletter" class="bg-bright-orange">
  <div class="container">
    <div class="row py-5 d-flex align-items-center">
      <div class="col-md-8 is-white">
        <h3>Connect With Us</h3>
        <p class="mb-0">Dapatkan update produk dan promo terbaru dari kami. Tulis emailmu di sini!</p>
      </div>
      <div class="col-md-4">
        <form action="#">
          {{ csrf_field() }}
          <div class="email-subscription d-flex justify-content-center">
            <input type="email" name="email" class="rounded-pill" placeholder="Email Address" required>
            <button type="submit" class="d-flex align-items-center justify-content-center py-2 px-2 size-7"><i class="fas fa-paper-plane mr-2"></i> Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<section id="footer" class="bg-gradient-blue is-white">
    <div class="container">
      <div class="row py-5">
        <div class="col-md-4 col-6">
          <h4>PT. Andalan Finance Indonesia Head Office</h4>
          <address>Jalan Sunburst CBD Lot II No. 3, BSD City, Lengkong Gudang, Kec. Serpong, Kota Tangerang Selatan, Banten 15321</address>
          <p><i class="fas fa-phone mr-2"></i> 021 1500 995</p>
          <p><i class="fas fa-envelope mr-2"></i> andalancare@andalanfinance.co.id</p>
        </div>
        <div class="col-md-4 col-6">
          <h4 class="text-center">Follow Us <br> On</h4>
          <div class="text-center">
            @foreach(getSocialMedia() as $item)
              <a href="{{ $item->setting_value ? $item->setting_value : '#' }}" target="_blank">
                @if ($item->setting_key == 'Facebook')  
                  <i class="size-3 mx-2 fab fa-facebook"></i>
                @elseif ($item->setting_key == 'Instagram')
                  <i class="size-3 mx-2 fab fa-instagram"></i>
                @elseif ($item->setting_key == 'Twitter')
                  <i class="size-3 mx-2 fab fa-twitter"></i>
                @elseif ($item->setting_key == 'Youtube')
                  <i class="size-3 mx-2 fab fa-youtube"></i>
                @endif
              </a>
            @endforeach
          </div>
        </div>
        <div class="col-md-4">
          <h4 class="text-center mt-4 mt-md-0">Download Aplikasi Andalanku <br> Sekarang!</h4>
          <div>
            <img src="{{ asset('images/device-p-review.png') }}" class="my-3 mx-auto">
          </div>
          <div class="Download-btns align-items-center">
            <a href="https://play.google.com/store/apps/details?id=id.andalan.andalanku" target="_blank">
              <img src="{{ asset('images/download-btns.png') }}" class="">
            </a>
          </div>
        </div>
        <div class="col-md-12 mt-md-5">
          <h2 class="heading-text size-3 has-line is-medium text-center mt-md-2 mt-5">OUR PARTNER</h2>
          <div class="row d-flex justify-content-center align-items-center">
            @foreach(getPartner() as $item)
                <div class="col-md-2 col-3">
                    <div class="card d-flex justify-content-center align-items-center partner my-md-5 my-2">
                        <a href="{{ $item->url != '' ? $item->url : '#' }}">
                            <img class="img-fluid rounded" src="{{ $item->image }}" alt="">
                        </a>
                    </div>
                </div>
            @endforeach
          </div>
        </div>
        <div class="col-md-12">
          <hr class="is-white">
          <div class="row">
            <div class="col-5 col-md-2 d-flex align-items-center pr-md-5">
              <img src="{{ asset('images/andalan-logo.png') }}" class="mr-md-3" alt="" width="50">
              <img src="{{ asset('images/logo-ojk-2.png') }}" alt="" width="50">
            </div>
            <div class="col-12 col-md-10 footer-link d-flex align-items-center justify-content-end size-7 mt-md-0 mt-3">
              <p class="is-pale-grey mb-0">&copy; © 2020 PT. Andalan Finance Indonesia | All Rights Reserved. | <a href="{{ route('term_and_conditions') }}">Term & Condition</a> | <a href="{{ route('privacy_policy') }}">Privacy Policy</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>