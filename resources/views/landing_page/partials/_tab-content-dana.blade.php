<div class="row">
  <div class="col-xs-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2">
    <h3 class="heading-text size-4 is-medium is-greyish-brown has-line py-3 mb-5">Kemudahan Pinjaman Multiguna untuk segala Keperluan dengan bunga bersaing dan proses Pencairan yang cepat.</h3>
    <!-- Tab Content Form -->
    <div class="tab-content__form" {{ $type == 'KMG'  ? 'hidden' : '' }}>
	@php
        $plafond = null;
        $dp_percent = null;
        $dp_rupiah = null;
        $tenor = 3;
        $tenor_slide = 0;
        $region = null;
        $insurance = null;

        if($simulation_result != '' && $type == 'KMG'){
          $plafond = $form_data->plafond;
          $dp_percent = $form_data->dp_percent;
          $dp_rupiah = $form_data->dp_rupiah;
          $tenor = $form_data->tenor;
          $tenor_slide = $form_data->tenor_slide;
          $region = $form_data->region;
          $insurance = $form_data->insurance;
        }
		else {
			$plafond = "";
          $dp_percent = "";
          $dp_rupiah ="";
          $tenor = "";
          $tenor_slide = "";
          $region = "";
          $insurance = "";
		}
      @endphp
      <form action="{{ route('simulation.post') }}" method="post" class="text-left" id="danaForm">
        <div class="form-row">
          <div class="form-group col-sm-12">
            <label for="">JUMLAH PLAFOND</label>
            <div class="input-group input-group-lg">
              <div class="input-group-prepend">
                <span class="input-group-text">Rp</span>
              </div>
              <input type="text" class="form-control" placeholder="Masukan Jumlah Plafond" name="plafond" id="dana_plafond">
            </div>
          </div>
          {{--<div class="form-group col-sm-12">--}}
            {{--<label for="">JAMINAN</label>--}}
            {{--<select class="form-control form-control-lg" name="" id="">--}}
              {{--<option></option>--}}
              {{--<option></option>--}}
            {{--</select>--}}
          {{--</div>--}}
          <div class="form-group col-sm-12">
            <label for="">JANGKA WAKTU</label>
            <select class="form-control form-control-lg" name="tenor" id="dana_tenor">
              <option value="" disabled selected >Pilih Jangka Waktu</option>
              <option value="3">3</option>
              <option value="6">6</option>
              <option value="12">12</option>
              <option value="18">18</option>
              <option value="24">24</option>
              <option value="30">30</option>
              <option value="36">36</option>
              <option value="48">48</option>
            </select>
          </div>
          <div class="form-group col-sm-12">
            <label class="d-flex align-items-center">ASURANSI JAMINAN</label>
            <select class="form-control form-control-lg" name="insurance" id="dana_insurance">
              <option value="" disabled selected>Pilih Asuransi</option>
              <option value="TLO">TLO</option>
              <option value="ARK">All Risk</option>
            </select>
          </div>
          <div class="form-group col-sm-12">
            <label for="">ZONA WILAYAH</label>
            <select class="form-control form-control-lg" name="region" id="dana_region">
              <option value="" disabled selected>Pilih Wilayah Domisili</option>
              <option value="1">Sumatra dan Kepulaunnya</option>
              <option value="2">Jakarta, Banten dan Jawa Barat</option>
              <option value="3">Lainnya</option>
            </select>
          </div>
          <input type="hidden" name="type" value="KMG">
          <input type="hidden" name="dp_percent" value="0">
          <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
          <div class="form-group col-sm-12 text-right">
            <button class="btn btn-secondary btn-lg mt-3" id="dana_btn_submit">HITUNG</button>
          </div>
        </div>
      </form>
      <ul class="list-unstyled mt-3 text-left size-7">
        <li>*Syarat dan ketentuan berlaku.</li>
        <li>*Simulasi merupakan bentuk perkiraan, bukan bentuk terakhir dari angsuran.</li>
        <li>*Harga dan bunga bisa berbeda sesuai dengan ketentuan per daerah.</li>
      </ul>
    </div>
    <!-- End of Tab Content Form -->

    <!-- Tab Content Result -->
    @if ($simulation_result != '' && $type == 'KMG')
    <div class="tab-content__result" id="KMG_content_result">
      <div class="mb-4">
        <label class="d-block text-bold is-azure mb-3">ESTIMASI ANGSURAN BULANAN</label>
        <span class="d-inline-block bg-white-three py-3 px-4 is-rounded-half size-2 is-bordered">Rp. {{ $simulation_result->angsuran}} / Bulan</span>
      </div>
      <div class="mb-4">
        <label class="d-block text-bold is-azure mb-3">JANGKA WAKTU</label>
        <span class="size-2">{{ $simulation_result->tenor }} Bulan</span>
      </div>
      
      <button class="btn btn-secondary btn-lg">HITUNG ULANG</button>

      <ul class="list-unstyled mt-5 text-left size-7">
        <li>*Syarat dan ketentuan berlaku.</li>
        <li>*Simulasi merupakan bentuk perkiraan, bukan bentuk terakhir dari angsuran.</li>
        <li>*Harga dan bunga bisa berbeda sesuai dengan ketentuan per daerah.</li>
      </ul>

      <hr class="my-5">

      <h3 class="heading-text size-4 is-medium is-greyish-brown has-line py-3 mb-5">Ajukan Pembiayaan untuk mendapatkan Mobil Impian anda</h3>
      <div class="col-xs-12 col-sm-8 offset-sm-2">
          <form action="{{ route('subscribe.post') }}"  method="post"  id="usedcarsSubmitRequest">
              <div class="mat-group input-group-lg">
                  <input type="text" class="form-control" name="name" id="usedcars_name" placeholder="Type your name">
              </div>
              <div class="mat-group input-group-lg">
                  <input type="text" class="form-control" name="email" id="usedcars_email" placeholder="Your E-Mail address">
              </div>
              <div class="mat-group input-group-lg">
                  <input type="text" class="form-control" name="phone" id="usedcars_phone" placeholder="Your Phone Number">
              </div>
			  <input type="hidden" name="product" value="DANA TUNAI">
			  <input type="hidden" name="otr" value="0">
			   <input type="hidden" name="plafond" value={{$plafond}}>
			  <input type="hidden" name="dp_percent" value="0">
			  <input type="hidden" name="dp_amount" value="0">
			  <input type="hidden" name="tenor" value={{$tenor}}>
			  <input type="hidden" name="insurance" value={{$insurance}}>
			  <input type="hidden" name="region" value={{$region}}>
	  <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
              <button class="btn btn-secondary btn-lg mt-4" id="newcars-submitnow-btn">AJUKAN SEKARANG</button>
          </form>
      </div>

    </div>
    @endif
    <!-- End of Tab Content Result -->
  </div>
</div>