@extends('landing_page.template')
@section('content')
<header class="bg-lightblue is-white pt-md-5" style="background-image:url({{ asset('images/bg-header-2.png') }})">
  <div class="container">
    <div class="text-center py-4">
      <h1 class="heading-text size-1 is-bold">ONE APPLICATION FOR ALL YOUR FINANCIAL NEEDS</h1>
      @include('landing_page.partials._faq_nav_menu', ['service' => 'faq'])
    </div>
  </div>
</header>

<section>
  <div class="container">
    <div class="row my-5">
      <div class="col-sm-12 text-center mt-5">
        <h2 class="heading-text is-azure has-line mb-5">Frequently Asked Questions</h2>
      </div>
      <div class="accordion w-100" id="accordionFaq">
        @foreach($faq_results as $key => $result)
          <div class="card faq-card">
            <a
              role="button"
              data-toggle="collapse"
              data-target="#collapse_{{ $key }}"
              aria-expanded="true"
              aria-controls="collapse_{{ $key }}"
            >
              <div class="card-header d-flex justify-content-between align-items-center" id="heading_{{ $key }}">
                {{ $key + 1}}. {{ $result->question }}
                <i class="fas fa-chevron-down" style="color: #009ae3"></i>
              </div>
            </a>
        
            <div id="collapse_{{ $key }}" class="collapse" aria-labelledby="heading_{{ $key }}" data-parent="#accordionFaq">
              <div class="card-body">
                <ol class="mb-0">
                  @foreach ($result->answers as $item)
                    <li>{{ $item->description}}</li>  
                  @endforeach
                </ol>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </div>
</section>
@endsection

@section('scripts')
  <script type="text/javascript">
    $('.collapse').on('shown.bs.collapse', function(){
      $(this).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
    }).on('hidden.bs.collapse', function(){
      $(this).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    });
  </script>
@endsection