@extends('landing_page.template')

@section('css')
<link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
@endsection

@section('content')
<header id="header" class="bg-lightblue is-white pt-md-5 py-5 py-md-0">
  <div class="container">
    <div class="row h-100 d-flex align-items-center">
      <div class="col-lg-7">
        <h2 class="header-title is-extrabold has-text-shadow">Satu Aplikasi Untuk Seluruh Kebutuhan Finansial Anda</h2>
        <p class="intro-text size-4">ANDALANKU hadir untuk memenuhi kebutuhan pembiayaan kendaraan roda empat atau lebih serta pembiayaan multiguna. ANDALANKU juga menyediakan layanan lainnya yang dibutuhkan konsumen dengan hanya beberapa langkah mudah</p>
      </div>
      <div class="col-lg-5 text-center">
        <img src="{{ asset('images/about-us.png') }}" alt="">
        <a href="#" class="btn-img">
          <img src="{{ asset('images/playstore.png') }}" alt="">
        </a>
      </div>
    </div>
  </div>
</header>

<section class="py-5 overflow-hidden pb-10">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <img src="{{ asset('images/product-image-1.png') }}" class="img-fluid" alt="">
      </div>
      <div class="col-lg-6">
        <h2 class="header-title font-weight-bold size-2 has-line-left mt-3 mt-md-0 mb-md-5 mb-3">E-Complain</h2>
        <p class="intro-text size-4 font-weight-bold">Mengajukan keluhan kini lebih Mudah.</p>
        <p>“Kami selalu berusaha mengevaluasi kinerja kami. Oleh sebab itu, keluhan anda sangatlah penting untuk kami agar kami dapat memberikan pelayanan yang lebih baik lagi. Hanya dengan beberapa langkah keluhan ada akan kami proses secepatnya.”</p>
      </div>
    </div>
  </div>
  <div class="swiper-container slider-how-to bg-pale-grey">
    <div class="swiper-wrapper py-5">
      <div class="swiper-slide">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <h3 class="header-title font-weight-bold is-lightblue size-2">HOW TO USE <br>E-COMPLAIN</h3>
              <p class="intro-text font-weight-bold size-4"><span class="is-bright-orange">Step 1</span> Klik menu E-Services <br>di halaman Home</p>
              <p>Pilih menu E-Services pada halaman Home Aplikasi ANDALANKU, kemudian Pilih E-Complain untuk pengajuan keluhan Anda.</p>
            </div>
            <div class="col-lg-5 position-relative">
              <img src="{{ asset('images/howto-ecomplain-1.png') }}" class="swiper-featured-img">
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <h3 class="header-title font-weight-bold is-lightblue size-2">HOW TO USE <br>E-COMPLAIN</h3>
              <p class="intro-text font-weight-bold size-4"><span class="is-bright-orange">Step 2</span> Klik E-Complain</p>
              <p>Pilih E-Complain untuk penyampaian Keluhan, dan mengetahui historis penangan dari keluhan yang Anda ajukan melalui Aplikasi ANDALANKU.</p>
            </div>
            <div class="col-lg-5 position-relative">
              <img src="{{ asset('images/howto-ecomplain-2.png') }}" class="swiper-featured-img">
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <h3 class="header-title font-weight-bold is-lightblue size-2">HOW TO USE <br>E-COMPLAIN</h3>
              <p class="intro-text font-weight-bold size-4"><span class="is-bright-orange">Step 3</span> Pilihlah AJUKAN KOMPLAIN dengan menekan tombol “+” di pojok kanan atas</p>
              <p>Jika Anda ingin menyampaikan keluhan untuk yang pertama kalinya pilih button AJUKAN KOMPLAIN, atau bisa juga tekan button Icon “+” pada Navigasi Bar untuk membuat pengajuan keluhan melalui Aplikasi ANDALANKU.</p>
            </div>
            <div class="col-lg-5 position-relative">
              <img src="{{ asset('images/howto-ecomplain-3.png') }}" class="swiper-featured-img">
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <h3 class="header-title font-weight-bold is-lightblue size-2">HOW TO USE <br>E-COMPLAIN</h3>
              <p class="intro-text font-weight-bold size-4"><span class="is-bright-orange">Step 4</span> Lengkapi FORM KELUHAN</p>
              <p>Isi dengan lengkap Form Keluhan beserta lampirkan file gambar / Foto bukti pendukung atas keluhan yang Anda sampaikan melalui Aplikasi ANDALANKU.</p>
            </div>
            <div class="col-lg-5 position-relative">
              <img src="{{ asset('images/howto-ecomplain-4.png') }}" class="swiper-featured-img">
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Add Arrows -->
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
  </div>
</section>

<section class="py-5 overflow-hidden pb-10">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <img src="{{ asset('images/product-image-2.png') }}" class="img-fluid" alt="">
      </div>
      <div class="col-lg-6">
        <h2 class="header-title font-weight-bold size-2 has-line-left mt-3 mt-md-0 mb-md-5 mb-3">E-Claim</h2>
        <p class="intro-text size-4 font-weight-bold">Proses klaim asuransi sekarang lebih singkat dan sederhana.</p>
        <p>Melalui Mobile Apps Andalanku, Kini berbagai claim asuransi kendaraan anda dapat kami proses dengan beberapa tahapan saja</p>
      </div>
    </div>
  </div>
  <div class="swiper-container slider-how-to bg-pale-grey">
    <div class="swiper-wrapper py-5">
      <div class="swiper-slide">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <h3 class="header-title font-weight-bold is-lightblue size-2">HOW TO USE <br>E-CLAIM ASURANSI</h3>
              <p class="intro-text font-weight-bold size-4"><span class="is-bright-orange">Step 1</span> Klik menu E-Services <br>di halaman Home</p>
              <p>Pilih menu E-Services pada halaman Home Aplikasi ANDALANKU, kemudian Pilih E-Claim Asuransi untuk pengajuan klaim atas premi asuransi Anda. </p>
            </div>
            <div class="col-lg-5 position-relative">
              <img src="{{ asset('images/howto-ecomplain-1.png') }}" class="swiper-featured-img">
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <h3 class="header-title font-weight-bold is-lightblue size-2">HOW TO USE <br>E-CLAIM ASURANSI</h3>
              <p class="intro-text font-weight-bold size-4"><span class="is-bright-orange">Step 2</span> Klik E-Claim Asuransi</p>
              <p>Pilih E-Claim Asuransi untuk pengajuan klaim asuransi atas premi yang anda miliki melalui Aplikasi ANDALANKU.</p>
            </div>
            <div class="col-lg-5 position-relative">
              <img src="{{ asset('images/howto-eclaim-2.png') }}" class="swiper-featured-img">
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <h3 class="header-title font-weight-bold is-lightblue size-2">HOW TO USE <br>E-CLAIM ASURANSI</h3>
              <p class="intro-text font-weight-bold size-4"><span class="is-bright-orange">Step 3</span> Pilihlah AJUKAN KLAIM dengan menekan tombol “+” di pojok kanan atas</p>
              <p>Jika Anda ingin menyampaikan keluhan untuk yang pertama kalinya pilih button AJUKAN KLAIM, atau bisa juga tekan button Icon “+” pada Navigasi Bar untuk membuat pengajuan keluhan melalui Aplikasi ANDALANKU.</p>
            </div>
            <div class="col-lg-5 position-relative">
              <img src="{{ asset('images/howto-eclaim-3.png') }}" class="swiper-featured-img">
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <h3 class="header-title font-weight-bold is-lightblue size-2">HOW TO USE <br>E-CLAIM ASURANSI</h3>
              <p class="intro-text font-weight-bold size-4"><span class="is-bright-orange">Step 4</span> Lengkapi FORM KLAIM ASURANSI</p>
              <p>Isi dengan lengkap Form Klaim Asuransi beserta lampirkan file gambar / foto bukti pendukung atas klaim Asuransi yang Anda sampaikan melalui Aplikasi ANDALANKU.</p>
            </div>
            <div class="col-lg-5 position-relative">
              <img src="{{ asset('images/howto-eclaim-4.png') }}" class="swiper-featured-img">
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <h3 class="header-title font-weight-bold is-lightblue size-2">HOW TO USE <br>E-CLAIM ASURANSI</h3>
              <p class="intro-text font-weight-bold size-4"><span class="is-bright-orange">Step 5</span> Klik KIRIM dan klaim anda akan kami proses secepatnya</p>
              <p>Isi dengan lengkap Form Klaim Asuransi beserta lampirkan file gambar / foto bukti pendukung atas klaim Asuransi yang Anda sampaikan melalui Aplikasi ANDALANKU.</p>
            </div>
            <div class="col-lg-5 position-relative">
              <img src="{{ asset('images/howto-eclaim-5.png') }}" class="swiper-featured-img">
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Add Arrows -->
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
  </div>
</section>

<section class="py-5 overflow-hidden pb-10">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <img src="{{ asset('images/product-image-3.png') }}" class="img-fluid" alt="">
      </div>
      <div class="col-lg-6">
        <h2 class="header-title font-weight-bold has-line-left size-2 mt-3 mt-md-0 mb-md-5 mb-3">E-Request</h2>
        <p class="intro-text size-4 font-weight-bold">Layanan pengembalian BPKB Anda semakin mudah.</p>
        <p>“Kini Anda dapat mengajukan pengambilan jaminan BPKB setelah masa kontrak dan sisa piutang Anda sudah habis, tanpa harus mengunjungi kantor cabang kami. Pengajuan dapat dilakukan melalui proses mudah berikut ini“</p>
      </div>
    </div>
  </div>
  <div class="swiper-container slider-how-to bg-pale-grey">
    <div class="swiper-wrapper py-5">
      <div class="swiper-slide">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <h3 class="header-title font-weight-bold is-lightblue size-2">HOW TO USE <br>E-REQUEST PENGAMBILAN JAMINAN</h3>
              <p class="intro-text font-weight-bold size-4"><span class="is-bright-orange">Step 1</span> Klik menu E-Services <br>di halaman Home</p>
              <p>Pilih menu E-Services pada halaman Home Aplikasi ANDALANKU, kemudian Pilih E-Request Pengambilan Jaminan untuk pengajuan pengambilan jaminan Anda. </p>
            </div>
            <div class="col-lg-5 position-relative">
              <img src="{{ asset('images/howto-ecomplain-1.png') }}" class="swiper-featured-img">
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <h3 class="header-title font-weight-bold is-lightblue size-2">HOW TO USE <br>E-REQUEST PENGAMBILAN JAMINAN</h3>
              <p class="intro-text font-weight-bold size-4"><span class="is-bright-orange">Step 2</span> Klik E-Request</p>
              <p>Pilih E-Request untuk pengajuan pengambilan Jagunan yang anda miliki melalui Aplikasi ANDALANKU.</p>
            </div>
            <div class="col-lg-5 position-relative">
              <img src="{{ asset('images/howto-erequest-2.png') }}" class="swiper-featured-img">
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <h3 class="header-title font-weight-bold is-lightblue size-2">HOW TO USE <br>E-REQUEST PENGAMBILAN JAMINAN</h3>
              <p class="intro-text font-weight-bold size-4"><span class="is-bright-orange">Step 3</span> Pilihlah AJUKAN PENGAMBILAN JAMINAN dengan menekan tombol “+” di pojok kanan atas</p>
              <p>Jika Anda ingin menyampaikan permohonan Pengambilan Jaminan yang pertama kalinya pilih button AJUKAN AMBIL JAMINAN, atau bisa juga tekan button Icon “+” pada Navigasi Bar untuk membuat pengajuan keluhan melalui Aplikasi</p>
            </div>
            <div class="col-lg-5 position-relative">
              <img src="{{ asset('images/howto-erequest-3.png') }}" class="swiper-featured-img">
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <h3 class="header-title font-weight-bold is-lightblue size-2">HOW TO USE <br>E-REQUEST PENGAMBILAN JAMINAN</h3>
              <p class="intro-text font-weight-bold size-4"><span class="is-bright-orange">Step 4</span> Lengkapi FORM PENGAMBILAN JAMINAN</p>
              <p>Isi dengan lengkap Form Pengajuan Ambil Jaminan beserta lampirkan file gambar / foto bukti pendukung atas Pengajuan Ambil Jaminan yang Anda sampaikan melalui Aplikasi ANDALANKU.</p>
            </div>
            <div class="col-lg-5 position-relative">
              <img src="{{ asset('images/howto-erequest-4.png') }}" class="swiper-featured-img">
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <h3 class="header-title font-weight-bold is-lightblue size-2">HOW TO USE <br>E-REQUEST PENGAMBILAN JAMINAN</h3>
              <p class="intro-text font-weight-bold size-4"><span class="is-bright-orange">Step 5</span> Klik KIRIM dan pengajuan anda akan kami proses secepatnya</p>
              <p>Isi dengan lengkap Form Pengajuan Ambil Jaminan beserta lampirkan file gambar / foto bukti pendukung atas Pengajuan Ambil Jaminan yang Anda sampaikan melalui Aplikasi ANDALANKU.</p>
            </div>
            <div class="col-lg-5 position-relative">
              <img src="{{ asset('images/howto-erequest-5.png') }}" class="swiper-featured-img">
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Add Arrows -->
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
  </div>
</section>
@endsection

@section('scripts')
<script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
<script type="text/javascript">
  var mySwiper = new Swiper('.swiper-container', {
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    }
  });
</script>
@endsection