@extends('landing_page.template')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/slick/slick.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/slick/slick-theme.css') }}"/>
<style>
html {
scroll-behavior: smooth;
}
</style>
@endsection

<header id="header" class="bg-lightblue header-index is-white pt-md-5 position-relative">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-5 d-flex align-items-end">
        <img src="{{ asset('images/iphone-header.png') }}" alt="">
      </div>
      <div class="col-12 col-md-7">
        <h1 class="header-title is-extrabold has-text-shadow mt-4">Aplikasi Mobile ANDALANKU.ID</h1>
        <p class="header-description my-4">
          ANDALANKU.ID hadir sebagai aplikasi dengan fitur-fitur unggulan yang menawarkan kemudahan dalam pengajuan kredit kendaraan serta multiguna
        </p>
        <div class="row mb-5">
          <div class="col-6 col-md-5">
            <a href="#">
              <img src="{{ asset('images/appstore.png') }}" alt="">
            </a>
          </div>
          <div class="col-6 col-md-5">
            <a href="#">
              <img src="{{ asset('images/playstore.png') }}" alt="">
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>

<section id="intro">
  <div class="container">
    <div id = "verification_content" class="row p-md-5">
{{--      <div class="col-7 mx-auto mx-md-0 col-md-4">--}}
{{--        <img src="{{ asset('images/phone-assets.png') }}" alt="" class="mt-5">--}}
{{--      </div>--}}
      <div class="col-xs-12 col-sm-12 bg-white pt-md-5 pb-4" style="text-align: center">

        <div class="col-6 col-md-3 offset-md-3" style="width: 253px; margin-left: 0px; text-align: center;">
          <a href="#">
            {{--<img src="{{ asset('images/verification_success.png') }}" alt="" class="mt-5">--}}
          </a>
        </div>
        <h2 class="heading-text is-azure mb-5">Terima Kasih <br>Alamat Email Anda Telah Terverifikasi</h2>
        <p class="mb-0">
          Nikmati mudahnya layanan pembiayaan kredit untuk <br>
          mobil baru, mobil lama, lelang mobil, dan juga kredit multiguna <br>
          menggunakan aplikasi Andalanku
        </p>
        <h2 class="heading-text is-azure mb-5"></h2>
        <button onclick="location.href = '{{ route('index') }}';"class="btn btn-secondary btn-lg" id="btn_recount_used" style="width: 184px">OK</button>
      </div>
    </div>
  </div>
</section>

@section('scripts')
<script type="text/javascript" src="{{ asset('plugins/slick/slick.min.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('js/homepage.js') }}"></script>--}}
<script type="text/javascript">
  $(document).ready(function() {
    let element = document.getElementById("verification_content");

    element.scrollIntoView();
  });
</script>
@endsection