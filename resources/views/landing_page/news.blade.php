@extends('landing_page.template')
@section('content')
<header class="bg-lightblue is-white pt-md-5" style="background-image:url({{ asset('images/bg-header-2.png') }})">
  <div class="container">
    <div class="text-center py-4">
      <h1 class="heading-text size-1 is-bold">BERITA TERBARU <br> SEPUTAR ANDALANKU.ID</h1>
      @include('landing_page.partials._news_nav_menu', ['service' => 'news'])
    </div>
  </div>
</header>

<section>
  <div class="container">
    <div class="row my-5">
      <div class="col-sm-12 text-center mt-5">
        <h2 class="heading-text is-azure has-line mb-5">BERITA TERBARU</h2>
      </div>
      @foreach($result as $item)
        <div class="col-sm-4">
        <div class="card mb-4">
          <img class="card-img-top" src="{{ $item->image }}" alt="{{ $item->title }}">
          <div class="card-body">
            <p class="card-text">{{ ucwords($item->title) }} </p>
            <a href="{{ route('news.detail', [$item->slug]) }}" class="d-flex justify-content-center">READ MORE</a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>
@endsection