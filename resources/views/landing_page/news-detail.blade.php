@extends('landing_page.template', ['title'=> $title, 'image' => $image])

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/slick/slick.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/slick/slick-theme.css') }}"/>
@endsection

@section('content')
<header class="bg-lightblue is-white">
  <div class="container">
    <div class="text-center py-4">
      @if ($page_type == 'berita')
        <h1 class="heading-text size-1 is-bold">BERITA TERBARU <br> SEPUTAR ANDALANKU.ID</h1>
      @elseif ($page_type = 'promo')
        <h1 class="heading-text size-1 is-bold">PROMO ANDALANKU</h1>
      @endif

      @include('landing_page.partials._news_nav_menu', ['service' => 'news'])
    </div>
  </div>
</header>

<section>
  <div class="container">
    <div class="row my-5">
      <div class="col-md-12 mt-5">
        <h2 class="heading-text is-azure my-5">{{ ucwords($item->title) }}</h2>

        <div class="single-post bg-white-two">
          <div class="single-post__header">
            <img src="{{ $item->image }}" alt="">
          </div>
          <div class="single-post__content p-5">
            <p>
			        {!! $item->content !!}
            </p>
          </div>
          <div class="single-post__footer text-center p-5">
            <div class="sharethis-inline-share-buttons"></div>
          </div>
        </div>
      </div>

      <div class="col-md-12 mt-5">
        <div class="row">
          <div class="col-md-12 text-center py-5">
            <h3 class="heading-text has-line is-azure">{{ strtoupper($page_type) }} TERBARU</h3>
          </div>
        </div>
        <div class="row slider">
          @foreach($list as $_list)
          <div class="col-sm-12">
            <div class="card mb-4">
              <img class="card-img-top" src="{{ $_list->image  }}" alt="">
              <div class="card-body">
                <p class="card-text">{{ $_list->title }}</p>
                @if($page_type == 'promo')
                  <a href="{{ route('promo.detail', [$_list->slug]) }}" class="d-flex justify-content-center">READ MORE</a>
                @elseif($page_type == 'berita')
                  <a href="{{ route('news.detail', [$_list->slug]) }}" class="d-flex justify-content-center">READ MORE</a>
                @endif
              </div>
            </div>
          </div>
          @endforeach

        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('plugins/slick/slick.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.slider').slick({
      infinite: true,
      speed: 300,
      slidesToShow: 3
    });
  });
</script>
<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5d148b3ff55c0f00125670de&product=inline-share-buttons' async='async'></script>
@endsection