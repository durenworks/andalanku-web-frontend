@extends('landing_page.template')
@section('content')
<header id="header" class="bg-lightblue is-white pt-md-5 py-5 py-md-0">
  <div class="container">
    <div class="h-100 d-flex align-items-center">
      <div class="row">
        <div class="col-lg-7">
          <h2 class="header-title is-extrabold has-text-shadow">Satu Aplikasi Untuk Seluruh Kebutuhan Finansial Anda</h2>
          <p class="intro-text size-4">ANDALANKU hadir untuk memenuhi kebutuhan pembiayaan kendaraan roda empat atau lebih serta pembiayaan multiguna. ANDALANKU juga menyediakan layanan lainnya yang dibutuhkan konsumen dengan hanya beberapa langkah mudah</p>
        </div>
        <div class="col-lg-5 text-center">
          <img src="{{ asset('images/about-us.png') }}" alt="">
          <a href="#" class="btn-img">
            <img src="{{ asset('images/playstore.png') }}" alt="">
          </a>
        </div>
      </div>
    </div>
  </div>
</header>

<section id="awards" class="bg-bright-grey">
  <div class="container">
    <img src="{{ asset('images/awards.png') }}" alt="">
  </div>
</section>

<section id="about" class="bg-white">
  <div class="container">
    <img src="{{ asset('images/about-andalan-finance.png') }}" alt="">
  </div>
</section>

<section id="feature" class="bg-bright-grey py-5 is-brownish-grey">
  <div class="container">
    <div class="row py-5">
      <div class="col-sm-12 text-center mb-5">
        <h2 class="heading-text is-azure">FITUR ANDALANKU.ID</h2>
        <p class="heading-text size-5 py-3">Berbagai fitur yang akan memudahkan anda</p>
      </div>
      <!-- Left Side -->
      <div class="col-12 col-md-4">
        <div class="row">
          <div class="col-sm-12 mb-4">
            <div class="row">
              <div class="col-10 text-right">
                <h5 class="is-azure is-bold">Plafond Kredit</h5>
                <p>Kamu bisa mengetahui perkiraan plafond pinjaman yang diberikan hanya dengan registrasi dan mengisi data diri.</p>
              </div>
              <div class="col-2">
                <img src="{{ asset('images/fitur-money-bag.svg') }}" alt="">
              </div>
            </div>
          </div>
          <div class="col-sm-12 mb-4">
            <div class="row">
              <div class="col-10 text-right">
                <h5 class="is-azure is-bold">Pembelian Kendaraan</h5>
                <p>Mau beli mobil baru atau bekas? Gampang kok, kamu bisa cek caranya disini</p>
              </div>
              <div class="col-2">
                <img src="{{ asset('images/fitur-shape.svg') }}" alt="">
              </div>
            </div>
          </div>
          <div class="col-sm-12 mb-4">
            <div class="row">
              <div class="col-10 text-right">
                <h5 class="is-azure is-bold">Pembiayaan Multiguna</h5>
                <p>Punya kebutuhan mendesak? Kamu bisa ajukan kapanpun dan dimanapun.</p>
              </div>
              <div class="col-2">
                <img src="{{ asset('images/fitur-ux.svg') }}" alt="">
              </div>
            </div>
          </div>
          <div class="col-sm-12 mb-4">
            <div class="row">
              <div class="col-10 text-right">
                <h5 class="is-azure is-bold">Reminder Jatuh Tempo Angsuran</h5>
                <p>Supaya kamu ngga kena denda, kita bantu ingetin pembayaran angsurannya.</p>
              </div>
              <div class="col-2">
                <img src="{{ asset('images/fitur-time.svg') }}" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End of Left Side -->

      <!-- Image Side -->
      <div class="col-12 col-md-4 xs-hidden">
        <img src="{{ asset('images/slider-bg-1.png') }}" alt="">
      </div>
      <!-- End of Image Side -->

      <!-- Right Side -->
      <div class="col-12 col-md-4">
        <div class="row">
          <div class="col-sm-12 mb-4">
            <div class="row">
              <div class="col-2">
                <img src="{{ asset('images/fitur-communication.svg') }}" alt="">
              </div>
              <div class="col-10">
                <h5 class="is-azure is-bold">Klaim Asuransi Kendaraan</h5>
                <p>Mobil kamu rusak karena kecelakaan atau hilang dicuri? Yuk langsung lapor aja via aplikasi ANDALANKU.ID, gampang kok!</p>
              </div>
            </div>
          </div>
          <div class="col-sm-12 mb-4">
            <div class="row">
              <div class="col-2">
                <img src="{{ asset('images/fitur-agile.svg') }}" alt="">
              </div>
              <div class="col-10">
                <h5 class="is-azure is-bold">Pengambilan BPKB</h5>
                <p>Angsuran sudah lunas tapi masih bingung nyari waktu yang pas buat ambil BPKB? Pesen aja dulu… kamu yang tentuin tanggalnya.</p>
              </div>
            </div>
          </div>
          <div class="col-sm-12 mb-4">
            <div class="row">
              <div class="col-2">
                <img src="{{ asset('images/fitur-ui.svg') }}" alt="">
              </div>
              <div class="col-10">
                <h5 class="is-azure is-bold">Mau Punya Uang Jajan Tambahan?</h5>
                <p>Kalau punya kerabat atau keluarga yang ada kebutuhan pembiayaan, referensikan aja di sini , nanti kita kasih bonus lho...</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End of Right Side -->
    </div>
  </div>
</section>
@endsection