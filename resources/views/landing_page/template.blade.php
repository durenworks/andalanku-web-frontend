<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta property="og:title" content="{{ isset($title) ? $title : 'Andalanku' }}" />
  <meta property="og:image" itemprop="image" content="{{ isset($image) ? $image :  asset('images/andalan-logo.png') }}">
  <meta property="og:updated_time" content="1440432930" />
  <title>Andalanku.id Web</title>
  <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous" async>
  <link rel="stylesheet" href="{{ asset('css/landing.css') }}">
  <link rel="stylesheet" href="{{ mix('css/style.css')}}">
  <style>
    html {
      scroll-behavior: smooth;
    }
  </style>
  <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5d148b3ff55c0f00125670de&product=inline-share-buttons' async='async'></script>
  @yield('css')
</head>
<body>
  @include('landing_page.partials._navbar')

  @yield('content')

  <div class="social-links">
    <a href="{{ session('facebook_url') == '' ? '#' : session('facebook_url') }}" target="_blank"><i class="fab fa-facebook"></i></a>
    <a href="{{ session('instagram_url') == '' ? '#' : session('instagram_url') }}" target="_blank"><i class="fab fa-instagram"></i></a>
    <a href="{{ session('twitter_url') == '' ? '#' : session('twitter_url') }}" target="_blank"><i class="fab fa-twitter"></i></a>
    <a href="{{ session('youtube_url') == '' ? '#' : session('youtube_url') }}" target="_blank"><i class="fab fa-youtube"></i></a>
  </div>

  @include('landing_page.partials._footer')

  @yield('modals')

  <div class="modal fade" id="joinModal" tabindex="-1" role="dialog" aria-labelledby="joinModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-body p-0">
          <div class="d-lg-flex">
            <div class="col-sm-12 col-lg-6 p-5 bg-lightblue text-white">
              <p class="header-title size-3 font-weight-bold">Let's Talk</p>
              <p class="size-5">1500995</p>
              <p>andalancare@andalanfinance.co.id</p>

              <p>Follow Us On</p>
              <div>
                <a href="{{ session('facebook_url') == '' ? '#' : session('facebook_url') }}" target="_blank" class="text-white"><i class="size-3 mx-2 fab fa-facebook"></i></a>
                <a href="{{ session('instagram_url') == '' ? '#' : session('instagram_url') }}" target="_blank" class="text-white"><i class="size-3 mx-2 fab fa-instagram"></i></a>
                <a href="{{ session('twitter_url') == '' ? '#' : session('twitter_url') }}" target="_blank" class="text-white"><i class="size-3 mx-2 fab fa-twitter"></i></a>
                <a href="{{ session('youtube_url') == '' ? '#' : session('youtube_url') }}" target="_blank" class="text-white"><i class="size-3 mx-2 fab fa-youtube"></i></a>
              </div>
            </div>
            <div class="col-sm-12 col-lg-6 p-5">
              <p class="size-7 font-weight-bold">Kita mulai dengan nama Anda?</p>
              <form action="{{ route('subscribe.post') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                  <input type="text" name="name" class="form-control border-left-0 border-right-0 border-top-0 rounded-0" placeholder="Type Your Name">
                </div>
                <div class="form-group">
                  <input type="text" name="email" class="form-control border-left-0 border-right-0 border-top-0 rounded-0" placeholder="Your Email Address">
                </div>
                <div class="form-group">
                  <input type="text" name="phone" class="form-control border-left-0 border-right-0 border-top-0 rounded-0" placeholder="Your Phone Number">
                </div>
                <button type="submiit" class="btn btn-secondary rounded-pill float-right">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" role="dialog" id="thanks">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <img src="{{ asset('images/subscribed-icon.png') }}" class="my-3 mx-auto">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-center mb-4">
          <h4 class="heading-text size-6 is-bold is-azure py-2">BERHASIL</h4>
      
        @if (session()->get('sender') == 'application')
          <P class="size-7 px-md-5">Pengajuan kamu sukses, dalam beberapa jam ke depan petugas kami akan menghubungi untuk proses lebih lanjut</P>
        @else
          <P class="size-7 px-md-5">Terima kasih atas minat Anda, Anda akan menjadi yang pertama tahu tentang berita, promo dan hadiah khusus dari ANDALANKU.ID.</P>
        @endif
      
        
          <button class="btn btn-secondary btn-sm px-5 rounded-pill" data-dismiss="modal" aria-label="Close">STAY TUNED</button>
        </div>
      </div>
    </div>
  </div>

  <div class="ld">
    <div class="loading"><div></div><div></div></div>
  </div>

  <!-- <a href="https://api.whatsapp.com/send?phone={{ session('whatsapp_number') }}" class="float" target="_blank">
    <i class="fab fa-whatsapp my-float"></i>
  </a> -->

  <script src="{{ asset('js/app.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function() {

      @if (session('subscribed'))
        $('#thanks').modal('show');
      @endif

      $('select.form-control').on('change', function() {
        $(this).css('color','#000000');
      });

      $( '#navbar-top' ).on( 'show.bs.dropdown', function() {
          $ ( 'body' ).css('overflow','hidden');
      });

      $( '#navbar-top' ).on( 'hide.bs.dropdown', function() {
          $ ( 'body' ).css('overflow','unset');
      });

    });

    $(window).scroll(function() {
      var scroll = $(window).scrollTop();

      if (scroll >= 100) {
        $('.navbar-top').addClass('shadow');
      } else {
        $('.navbar-top').removeClass('shadow');
      }
    });
  </script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-151804124-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-151804124-1');
  </script>
  <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '2137015046605228');
  fbq('track', 'PageView');
</script>
<noscript>
  <img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=2137015046605228&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
  @yield('scripts')
 
<!--Start of Tawk.to Script-->
<script type="text/javascript">
  var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
  (function(){
  var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
  s1.async=true;
  s1.src='https://embed.tawk.to/5e65d8728d24fc2265867a43/default';
  s1.charset='UTF-8';
  s1.setAttribute('crossorigin','*');
  s0.parentNode.insertBefore(s1,s0);
  })();
  </script>
  <!--End of Tawk.to Script-->
</body>
</html>
