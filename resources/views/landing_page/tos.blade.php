@extends('landing_page.template')
@section('content')
<header class="bg-lightblue header-bg-3 is-white pt-md-5" style="background-image:url({{ asset('images/bg-header-2.png') }})">
  <div class="container">
    <div class="text-center py-4">
      <h1 class="heading-text size-1 is-bold my-md-5">ONE APPLICATION FOR ALL <br> YOUR FINANCIAL NEEDS</h1>
      @include('landing_page.partials._faq_nav_menu', ['service' => 'term_and_conditions'])
    </div>
  </div>
</header>

<section>
  <div class="container">
    <div class="row my-5">
      <div class="col-sm-12 text-center mt-5">
        <h2 class="heading-text is-azure has-line mb-5">SYARAT & KETENTUAN</h2>
      </div>
      <div class="col-sm-12">
        {!! $tos !!}
      </div>
    </div>
  </div>
</section>
@endsection