@extends('landing_page.template')
@section('content')
<header class="bg-lightblue header-bg-3 is-white pt-md-5" style="background-image:url({{ asset('images/bg-header-2.png') }})">
  <div class="container">
    <div class="text-center py-4">
      <h1 class="heading-text size-1 is-bold my-md-5">ONE APPLICATION FOR ALL <br> YOUR FINANCIAL NEEDS</h1>
      @include('landing_page.partials._faq_nav_menu', ['service' => 'privacy_policy'])
    </div>
  </div>
</header>

<section>
  <div class="container">
    <div class="row my-5">
      <div class="col-sm-12 text-center mt-5">
        <h2 class="heading-text is-azure has-line mb-5">KEBIJAKAN PRIVASI</h2>
      </div>
      <div class="col-sm-12">
        <p>
          Dengan mengakses dan menggunakan ANDALANKU.ID, anda dianggap telah membaca, memahami dan menyetujui ketentuan mengenai penerimaan, penggunaan dan pengungkapan informasi yang anda berikan kepada kami, PT Andalan Finance Indonesia, di bawah ini. Dari waktu ke waktu, kami dapat melakukan perubahan Kebijakan Privasi tanpa memberikan pemberitahuan lebih dulu mengenai perubahan tersebut kepada anda. Anda diharapkan membaca Kebijakan Privasi kami dari waktu ke waktu untuk mengetahui adanya perubahan pada Kebijakan Privasi. <br>
        </p>
        <p>
          A. INFORMASI <br>
          <ol>
            <li>Ketika anda membuka akun pada ANDALANKU.ID, anda mengetahui dan menyetujui bahwa kami akan melakukan penerimaan, penyimpanan dan pengolahan informasi anda termasuk namun tidak terbatas pada nama, alamat, alamat email, tanggal lahir, pekerjaan, data finansial, nomor telepon, nama pengguna serta kata sandi, rekaman dan rincian komunikasi anda dengan kami serta informasi lain yang perlu untuk mengoperasikan akun anda pada ANDALANKU.ID (selanjutnya disebut “Informasi Pribadi”).</li>
            <li>Selama anda menggunakan ANDALANKU.ID, kami menerima, menyimpan dan mengolah informasi penggunaan ANDALANKU.ID oleh anda, termasuk namun tidak terbatas pada konten yang anda akses, pencarian yang anda lakukan, fasilitas yang anda gunakan dan tindakan lainnya yang anda lakukan. Data mengenai penggunaan ANDALANKU oleh anda akan kami simpan, termasuk namun tidak terbatas pada alamat Internet Protocol (IP), gawai, jenis browser, halaman yang anda kunjungi, waktu dan tanggal akses (selanjutnya disebut “Informasi Penggunaan”)</li>
          </ol>
        </p>
        <p>
          A. Penggunaan dan Pengungkapan Informasi <br>
          <ol>
            <li>Kami menggunakan Informasi Pribadi dan Informasi Penggunaan untuk optimalisasi penggunaan ANDALANKU.ID oleh anda, melakukan analisa guna pengembangan produk dan layanan kami, meningkatkan personalisasi penggunaan ANDALANKU.ID oleh anda, termasuk namun tidak terbatas pada konten dan iklan yang akan anda akses.</li>
            <li>Kami dapat Informasi Pribadi dan Informasi Penggunaan untuk mengirimkan kepada anda informasi tentang ANDALANKU.ID dan materi pemasaran yang relevan bagi Anda. Setiap saat anda dapat meminta untuk menghentikan penerimaan informasi yang kami maksud pada nomor 2 ini dengan menggunakan tombol penghentian langganan dalam informasi yang kami kirimkan.</li>
            <li>Kami tidak akan mengungkapkan informasi apa pun yang kami terima dari anda keluar dari kelompok usaha kami kecuali kepada pemerintah atau aparat penegak hukum dalam rangka proses hukum atau pemenuhan kewajiban berdasarkan peraturan yang berlaku. </li>
            <li>Kami akan mengungkapkan Informasi Pribadi dan Informasi Penggunaan kepada pihak ketiga yang kami pekerjakan dan/atau melakukan kerjasama dengan kami dan dalam proses pelaksanaan pekerjaan dan/atau kerjasama dengan kami (selanjutnya disebut “Rekanan ANDALANKU.ID”). </li>
            <li>Rekanan ANDALANKU.ID yang menerima Informasi Pribadi dan Informasi Penggunaan, wajib untuk menjaga kerahasiaan informasi dan tidak mengungkapkan dan/atau menggunakan informasi selain guna pelaksanaan pekerjaan dan/atau kerjasama dengan kami.</li>
            <li>Kami dapat menjual dan/atau mengalihkan aset kami, termasuk Informasi Pribadi dan Informasi Penggunaan sebagaimana disebutkan dalam Kebijakan Privasi ini, sehubungan dengan suatu aktivitas bisnis yang kami lakukan termasuk namun tidak terbatas pada merger, akuisisi, reorganisasi atau penjualan aset.</li>
          </ol>
        </p>
      </div>
    </div>
  </div>
</section>
@endsection