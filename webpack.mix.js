let mix = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
  .js('resources/assets/js/app.js', 'public/js')
  .js('resources/assets/js/homepage.js', 'public/js')
  .js('resources/assets/js/footer.js', 'public/js')
  .sass('resources/assets/sass/landing_page/landing.scss', 'public/css/')
  .styles(['resources/assets/css/style.css'], 'public/css/style.css')
  .copyDirectory('resources/assets/images', 'public/images')
  // Slick
  .copyDirectory(
    'resources/assets/plugins/slick/fonts/',
    'public/plugins/slick/fonts'
  )
  .copy('resources/assets/plugins/slick/slick.css', 'public/plugins/slick')
  .copy('resources/assets/plugins/slick/slick.min.js', 'public/plugins/slick')
  .sass(
    'resources/assets/plugins/slick/slick-theme.scss',
    'public/plugins/slick/'
  )
  // Versioning
  .version()
  .browserSync({
    proxy: 'http://localhost:8000'
  })
